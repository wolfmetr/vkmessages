//
//  VkUser.h
//  VkMessages
//
//  Created by baistore on 06.03.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
@class VkDelegate;
@interface VkUser : NSObject


@property NSString *firstName;
@property NSString *lastName;
@property NSString *screenName;
@property NSString *nickname;
@property NSString *uid;
@property NSString *photo;
@property NSString *photo_100;


+(id) initByUid:(NSString *) uid;
+(id) initByDictionary:(NSDictionary *) dictionary;
+(id) initByUser:(VkUser *) user;
+(NSArray*) loadByUid:(NSString *) uid;
-(void) setPhoto:(NSImage *)photo withURL:(NSString *)url;

@end

//
//  PreferencesWindowController.h
//  VkMessages
//  Created by wolfmetr on 17.01.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

//	Preferences for application
#import <Cocoa/Cocoa.h>

extern NSString * const VKCUpdateTimeKey;
extern NSString * const VKUpdateTimeChangedNotification;
@class vkAppDelegate;

@interface PreferencesWindowController : NSWindowController{
	NSNumber *updateTime;
	IBOutlet NSTextField *secondTime;
	//vkAppDelegate *vkApp;
}
+ (NSNumber*) preferenceUpdateTime;
+ (void) setPreferenceUpdateTime:(NSNumber*)time;

- (IBAction)changeUpdateTime:(id)sender;
- (IBAction)testButton:(id)sender; 


@end

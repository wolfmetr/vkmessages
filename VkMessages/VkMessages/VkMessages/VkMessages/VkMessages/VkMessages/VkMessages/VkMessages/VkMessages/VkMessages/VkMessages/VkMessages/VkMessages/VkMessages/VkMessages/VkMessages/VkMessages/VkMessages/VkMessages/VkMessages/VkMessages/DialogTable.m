//
//  DialogTable.m
//  VkMessages
//
//  Created by baistore on 24.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "DialogTable.h"

@implementation DialogTable

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

@end

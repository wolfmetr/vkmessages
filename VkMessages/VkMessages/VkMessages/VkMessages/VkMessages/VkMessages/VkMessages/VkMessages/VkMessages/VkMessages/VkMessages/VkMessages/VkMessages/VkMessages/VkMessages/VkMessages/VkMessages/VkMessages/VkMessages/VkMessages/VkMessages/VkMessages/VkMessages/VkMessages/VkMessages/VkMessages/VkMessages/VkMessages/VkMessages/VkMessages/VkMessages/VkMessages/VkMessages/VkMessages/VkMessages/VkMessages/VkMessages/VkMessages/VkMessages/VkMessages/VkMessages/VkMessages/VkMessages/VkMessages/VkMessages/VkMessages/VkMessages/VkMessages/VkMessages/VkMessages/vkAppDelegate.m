//
//  vkAppDelegate.m
//  VkMessages
//
//  Created by baistore on 17.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import "vkAppDelegate.h"
#import "VkMessage.h"
#import "PreferencesWindowController.h"
#import "DialogsViewController.h"
#import <WebKit/WebKit.h>
#import "VkUser.h"
@implementation vkAppDelegate
@synthesize messages = messages_;
@synthesize dialogTable = _dialogTable;
//@synthesize dialogTable;

-(id)init{
	self = [super init];
    if (self) {
		updateTime = [[[NSUserDefaults standardUserDefaults] objectForKey:VKCUpdateTimeKey] intValue];
		NSNotificationCenter *nc =
		[NSNotificationCenter defaultCenter];
		[nc addObserver:self
			   selector:@selector(handleUpdateTimeChange:)
				   name:VKUpdateTimeChangedNotification
				 object:nil];
		_users = [[VkUsers alloc] init];
		_dialogs = [[VkDialogs alloc] init] ;

	}
	return self;
}
+ (void)initialize
{

	
	
	//[self setValue:[NSNumber numberWithInt:[updateTimeT intValue]] forKey:@"updateTime"];
	
	
		
	// Create a dictionary
	//NSMutableDictionary *defaultValues = [NSMutableDictionary dictionary];
	
	// Put defaults in the dictionary
	//[defaultValues setObject:[NSNumber numberWithInt:20] forKey:VKCUpdateTimeKey];
	
	// Register the dictionary of defaults
	//[[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
	//NSLog(@"registered defaults: %@", defaultValues);
}

- (void)handleUpdateTimeChange:(NSNotification *)note
{
	NSLog(@"Received notification: %@", note);
	NSNumber *time = [[note userInfo] objectForKey:@"time"];
	updateTime = [time intValue];
//	NSLog(@"Received notification:tr");
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	_men = [[Menu alloc] initTrayWithMenu:_trayMenu]; // Initializating (I've write custom init method) status bar icon w/ menu
	center = [NSUserNotificationCenter defaultUserNotificationCenter];
	[center setDelegate:self];
	dockTile = [NSApp dockTile];
	
	authWebView = [[WebView alloc] initWithFrame:authView.frame];
	[authView addSubview:authWebView];
	[authWebView setFrameLoadDelegate:self];
	NSString *urlAddress = [NSString stringWithFormat:vkURLRequest1,vkAPI_ID,vkRules];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [[authWebView mainFrame] loadRequest:requestObj];
	
	self.messages = [[VkMessages alloc] init];
	queueTimerUpdate = [[NSOperationQueue alloc] init];
}

/**************************
 Получение новых сообщений
 **************************/
- (void)checkMessage
{

	//	получаю список последних 10 диалогов
	self.dialogs = [[VkDelegate sharedInstance] getDialogs:10 from:1];
	
	// получить id юзеров из диалогов
	NSMutableSet *userIdsSet = self.dialogs.getUsersIdsSet;

	
	[self.users addUsersFromSetIds:userIdsSet];
	NSLog(@"ALL USERS: %@", self.users.users);
	
	[self.dialogs initUsersFormVkUsers:self.users];
	if (!dialogsViewController)
		dialogsViewController = (DialogsViewController*)_dialogTable.delegate;
	[[dialogsViewController dialogs] setDialogs:[self.dialogs dialogs]];
//	[[(DialogsViewController*)_dialogTable.delegate dialogs] setDialogs:[self.dialogs dialogs]];
	_dialogTable.reloadData;
	
	// получаю словарь сообщений с сервера
	NSMutableDictionary *messagesNew = (NSMutableDictionary *)[[VkDelegate sharedInstance] checkMessages];
	// количество сообщений
	countNewMessages = (NSInteger *)[[[messagesNew objectForKey:@"response"] objectAtIndex:0] integerValue];

	NSNumber *cnt = [NSNumber numberWithInteger:countNewMessages];
	// пересчитываем бейдж на иконке снизу
	[_men setMessage: [NSString stringWithFormat:@"%@",cnt]];
	if (cnt.intValue > 0)
		[dockTile setBadgeLabel:[NSString stringWithFormat:@"%@",cnt]];
	else
		[dockTile setBadgeLabel:@""];

	// заполнение класса хранилища сообщений
	for (int i = 1; i <= countNewMessages; i++){
		VkMessage *msg = [[VkMessage alloc] init];
		[msg setMessage:[[messagesNew objectForKey:@"response"] objectAtIndex:i]];
		BOOL contain = NO;
		// проверяем есть ли сообщение в списке
		for (VkMessage *m in [self.messages messages]){
			if ([[m mid] isEqual:msg.mid]){
				contain = YES;
				break;
			}	
		}
		// если сообщения нет, то добавляем
		if (!contain)
			[self.messages addMessage:msg];
	}
	
	
	/*
	 Для каждого сообщения получаем информацию
	 о его авторе и отправлем уведомление в UserNotificationCenter
	 */
	for (VkMessage *m in [self.messages messages]){
		VkUser *curUser;
		// если пользователя с таким uid ещё нет в словаре
		if (![self.users getUserByUid:m.uid])
		{
			// инициализируем пользователя
			curUser = [VkUser initByUid:m.uid];
			// добавляем пользователья в словарь
			[self.users addUser:curUser];
			NSLog(@"count users %lu", (unsigned long)[self.users countUsers]);
		} else {
			curUser = [self.users getUserByUid:m.uid];
		}
		
		NSLog(@"curUser = %@", [self.users getUserByUid:[m uid]]);
		//NSLog(@"self.users = %@", self.users.users);
		
		if (![m isReaded]){
			// отправляем сообщение в UserNotificationCenter
			[self sendNotification:[NSString stringWithFormat:@"%@",m.body ]
							author:[NSString stringWithFormat:@"%@",[[curUser.firstName stringByAppendingString:@" "] stringByAppendingString:curUser.lastName]]];
			[m setRead];
		}
	}
}




- (IBAction)send:(id)sender
{
	NSLog(@"SEND REQUEST");
	[self startPoll];
	
	
	/*NSMutableDictionary *userInfo = [[VkDelegate sharedInstance] userByUid:@"" fields:@"uid,first_name,last_name,screen_name,photo" name_case:@"nom"];
	NSLog(@"User: %@",userInfo);
	//	сделать класс для описания юзера,сделать метода для добавляения юзера к сообщению
	NSLog(@"%@",userInfo);
	[[VkDelegate sharedInstance] testQuery];*/
}

- (void) openDialogWithMid:(NSString *) midDialog
{
	NSLog(@"openDialogWithMid : %@", midDialog);
}

#pragma mark - Long pull actions

// костыль
- (void) longPollWrapper:(NSDictionary *)args {
	NSLog(@"longPollWrapper");
    [self longPoll:[NSString stringWithFormat:@"%@",[args objectForKey:@"server"]]
		   withKey:[NSString stringWithFormat:@"%@",[args objectForKey:@"key"] ]
	  andTimestamp:[NSString stringWithFormat:@"%@",[args objectForKey:@"ts"] ]];
}


- (void) longPoll:(NSString*) server
		  withKey:(NSString*) key
	 andTimestamp:(NSString*) ts
{
	NSLog(@"longPoll");
	
    //compose the request
    NSError* error = nil;
    NSURLResponse* response = nil;
    NSURL* requestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@?act=a_check&key=%@&ts=%@&wait=25&mode=0",server, key, ts]];
    NSURLRequest* request = [NSURLRequest requestWithURL:requestUrl];
	
    //send the request (will block until a response comes back)
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request
												 returningResponse:&response error:&error];
	//NSLog(@"%@",responseData);
    //pass the response on to the handler (can also check for errors here, if you want)
    [self performSelectorOnMainThread:@selector(dataReceived:)
						   withObject:responseData waitUntilDone:YES];
	
	NSDictionary *parsedData;
	
	if (responseData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
		
		NSDictionary * args = [NSDictionary dictionaryWithObjectsAndKeys:
							   [NSString stringWithString:server], @"server",
							   [NSString stringWithString:key], @"key",
							   [NSString stringWithString:[NSString stringWithFormat:@"%@",[parsedData objectForKey:@"ts"]]], @"ts",
							   nil];
		
		//send the next poll request
		 [self performSelectorInBackground:@selector(longPollWrapper:) withObject: args];
	} else {
		NSLog(@"Error with %@", error);
		[self startPoll];
	}
	
}

- (void) startPoll {
	NSLog(@"startPoll");
	NSDictionary* dict = [[[VkDelegate sharedInstance] getLongPullServer] objectForKey:@"response"];
	NSString *server = [NSString stringWithFormat:@"%@",[dict objectForKey:@"server"]];
	NSString *key = [NSString stringWithFormat:@"%@",[dict objectForKey:@"key"]];
	NSString *ts = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ts"]];

	NSDictionary * args = [NSDictionary dictionaryWithObjectsAndKeys:
						   [NSString stringWithString:server], @"server",
						   [NSString stringWithString:key], @"key",
						   [NSString stringWithString:ts], @"ts",
						   nil];
    //not covered in this example:  stopping the poll or ensuring that only 1 poll is active at any given time
    [self performSelectorInBackground:@selector(longPollWrapper:) withObject: args];
}

- (void) dataReceived: (NSData*) theData {
    //process the response here
	NSLog(@"process the response here");
	NSLog(@"%@",theData);
	
	NSError *error = nil;
	NSDictionary *parsedData;
	
	if (theData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:theData options:kNilOptions error:&error];
		NSLog(@"%@",parsedData);
	} else {
		NSLog(@"Error with %@", error);
	}
	
	NSLog(@"======================");
}




#pragma mark - WebView delegate
- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	//NSLog(@"WebView didFinishLoadForFrame");
	NSMutableDictionary* user = [[NSMutableDictionary alloc] init];
	NSString *currentURL = frame.webView.mainFrameURL; // получаем текущий URL
	NSRange textRange =[[currentURL lowercaseString] rangeOfString:[@"access_token" lowercaseString]];
	if(textRange.location != NSNotFound){
		NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];	
		//NSLog(currentURL);
        [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
        [user setObject:[data objectAtIndex:3] forKey:@"expires_in"];
        [user setObject:[data objectAtIndex:5] forKey:@"user_id"];

		if ([[VkDelegate sharedInstance] loginWithParams:user]){
			[authWebView removeFromSuperview];
			//[_window close];
			[self startTimer];
			
		} else {
			[authWebView removeFromSuperview];
		}
	} else {
		NSLog(@"VK: Access denied");
	}
	
}

#pragma mark - UserNotifications delegate
- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
	return YES;
}


/**
 * Action requires when user click to notification message
 */
- (void) userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification
{
	if ([notification activationType] == NSUserNotificationActivationTypeContentsClicked){
		[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.vk.com/im"]];
	}
	
}

/**
 *	Send new user-notification to system
 */
- (void) sendNotification: (NSString *) msg author:(NSString *) name
{
	NSUserNotification *notification = [[NSUserNotification alloc] init];
	//[notification setTitle:@"Новое сообщение VK"];
	[notification setTitle:name];
	//[notification setSubtitle:name];
	if ([msg isEqual:@""]) msg = @"Медиа контент";
	[notification setInformativeText:msg];
	[notification setDeliveryDate:[NSDate dateWithTimeInterval:5 sinceDate:[NSDate date]]];
	[notification setSoundName:NSUserNotificationDefaultSoundName];
	[center scheduleNotification:notification];
}

#pragma mark - Timer
-(void)startTimer
{
    [self checkMessage];
	//NSNumber *updateTime = [[NSUserDefaults standardUserDefaults] objectForKey:VKCUpdateTimeKey];
    _myTimer = [NSTimer scheduledTimerWithTimeInterval:updateTime target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
}

-(void) stopTimer
{
	[_myTimer invalidate];
}

-(void) timerFired:(NSTimer *)theTimer
{
	[queueTimerUpdate addOperationWithBlock:^{
		[progress startAnimation:nil];
		[self checkMessage];
		[progress stopAnimation:nil];
	}];
	
	if ((int)[_myTimer timeInterval] != updateTime){
		[_myTimer invalidate];
		_myTimer = nil;
		_myTimer = [NSTimer scheduledTimerWithTimeInterval:updateTime target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
	}
}

#pragma mark - Preferences
- (IBAction)showPreferencesPanel:(id)sender
{
	if(!preferences){
		preferences = [[PreferencesWindowController alloc] init];
	}
	[preferences showWindow:self];
	
}

#pragma mark - Menu
- (IBAction)toSite:(id)sender
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.vk.com/im"]];
}


@end

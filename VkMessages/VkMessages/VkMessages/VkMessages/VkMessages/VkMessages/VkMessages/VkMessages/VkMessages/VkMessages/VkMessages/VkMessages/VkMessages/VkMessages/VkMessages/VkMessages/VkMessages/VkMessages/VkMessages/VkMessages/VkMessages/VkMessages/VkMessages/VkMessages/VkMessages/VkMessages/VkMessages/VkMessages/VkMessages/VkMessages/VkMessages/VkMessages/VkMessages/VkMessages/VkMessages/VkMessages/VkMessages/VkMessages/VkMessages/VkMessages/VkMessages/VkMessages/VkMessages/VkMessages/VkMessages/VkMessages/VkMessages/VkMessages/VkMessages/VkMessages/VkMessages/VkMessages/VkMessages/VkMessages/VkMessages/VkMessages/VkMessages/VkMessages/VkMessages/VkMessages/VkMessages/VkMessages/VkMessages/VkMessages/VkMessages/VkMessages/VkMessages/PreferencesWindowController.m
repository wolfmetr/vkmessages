//
//  PreferencesWindowController.m
//  VkMessages
//
//  Created by baistore on 17.01.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "PreferencesWindowController.h"
NSString * const VKCUpdateTimeKey = @"VKCUpdateTimeKey";
NSString * const VKUpdateTimeChangedNotification = @"VKUpdateTimeChangedNotification";

@interface PreferencesWindowController ()

@end



@implementation PreferencesWindowController

+(void) initialize {
	
}
-(id) init{
	
	self = [super initWithWindowNibName:@"Preferences"];
	if (self) {
		NSMutableDictionary *defaultValues = [NSMutableDictionary dictionary];
		NSNumber *updateTimeDefault = [NSNumber numberWithInt:20];
		[defaultValues setObject:updateTimeDefault forKey:VKCUpdateTimeKey];
		
		[[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
    }
	return self;
}

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
	
	}
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
	[secondTime setIntegerValue:[[PreferencesWindowController preferenceUpdateTime] integerValue]];
}

- (IBAction)testButton:(id)sender{
	//NSLog(@"Test message");
}

- (IBAction)changeUpdateTime:(id)sender{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	NSLog(@"Sending notification");
	NSNumber *time = [NSNumber numberWithInt:[secondTime intValue]];
	NSDictionary *d = [NSDictionary dictionaryWithObject:time
												  forKey:@"time"];
	[nc postNotificationName:VKUpdateTimeChangedNotification
					  object:self
					userInfo:d];
	
	[PreferencesWindowController setPreferenceUpdateTime:[NSNumber numberWithInt:[secondTime intValue]]];
}


+ (NSNumber*) preferenceUpdateTime{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSNumber *updateTime = [defaults objectForKey:VKCUpdateTimeKey];
	return updateTime;
}
+(void) setPreferenceUpdateTime:(NSNumber *)time{
	[[NSUserDefaults standardUserDefaults] setObject:time
											  forKey:VKCUpdateTimeKey];
}


@end

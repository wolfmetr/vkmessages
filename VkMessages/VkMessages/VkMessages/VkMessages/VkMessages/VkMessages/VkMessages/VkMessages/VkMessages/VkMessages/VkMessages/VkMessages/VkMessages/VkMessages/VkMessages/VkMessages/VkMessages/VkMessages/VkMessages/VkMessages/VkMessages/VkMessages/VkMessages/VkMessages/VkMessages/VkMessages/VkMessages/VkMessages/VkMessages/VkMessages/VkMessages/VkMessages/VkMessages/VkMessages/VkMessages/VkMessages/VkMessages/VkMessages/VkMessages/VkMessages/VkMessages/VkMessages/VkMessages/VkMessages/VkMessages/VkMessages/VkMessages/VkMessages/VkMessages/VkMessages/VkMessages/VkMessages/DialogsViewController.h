//
//  DialogsViewController.h
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//@class VkDialog;
@class VkDialogs;
@interface DialogsViewController : NSObject <NSTableViewDataSource, NSTableViewDelegate>{
	VkDialogs *dialogs;
	IBOutlet NSTableView* table1; //таблица
}

@property (nonatomic, retain) VkDialogs *dialogs;

- (void) resetDialogs:(VkDialogs*)dlgs;

@end

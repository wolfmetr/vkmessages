//
//  VkDelegate.m
//  VkMessager
//
//  Created by baistore on 02.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import "VkDelegate.h"

@implementation VkDelegate{
	NSUserDefaults *userDefaults;
}
@synthesize username, realName, ID, photo, access_token, email, link;

+ (id)sharedInstance {
    static VkDelegate *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[VkDelegate alloc]init];
    });
    return __sharedInstance;
}

- (id) init {
	userDefaults = [NSUserDefaults standardUserDefaults];
	if([userDefaults objectForKey:@"countForCheckMsgs"])
		countForCheckMsgs = [NSNumber numberWithInteger:[[userDefaults objectForKey:@"countForCheckMsgs"] integerValue]];
	else
		countForCheckMsgs = [NSNumber numberWithInt:10];
    access_token = [userDefaults objectForKey:@"vk_token"];
    ID = [userDefaults objectForKey:@"vk_id"];
    return  self;
}



/**
 * Авторизация
 */
-(BOOL) loginWithParams:(NSMutableDictionary *)params {
    ID = [params objectForKey:@"user_id"];
    access_token = [params objectForKey:@"access_token"];
	NSLog(@"access_token = %@",access_token);//b3322f2332584eac20aaa3dfd9fd735ae68b9a9df1c9981f6e988264b876c323a417ebb36aede7da81aa5
	
    [userDefaults setValue:access_token forKey:@"vk_token"];
    [userDefaults setValue:ID forKey:@"vk_id"];
    [userDefaults synchronize];
	
    NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?uid=%@&access_token=%@", ID, access_token] ;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];	
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
	
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	// TODO: UTF8???
	NSDictionary *parsedData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
	NSDictionary *userData = [[parsedData objectForKey:@"response"] objectAtIndex:0];
	realName = [[[userData objectForKey:@"first_name"] stringByAppendingString:@" "] stringByAppendingString:[userData objectForKey:@"last_name"]];
    /*NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	
    NSArray* userData = [responseString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\":{},[]"]];
    realName = [userData objectAtIndex:14];
    realName = [realName stringByAppendingString:@" "];
    realName = [realName stringByAppendingString:[userData objectAtIndex:20]];
	 */
    [userDefaults setValue:@"vkontakte" forKey:@"SignedUpWith"];
    [userDefaults setValue:realName forKey:@"RealUsername"];
    return [userDefaults synchronize];
}

// Posts a MyNotification message whenever called
- (void)notify {
	[[NSNotificationCenter defaultCenter] postNotificationName:@"MyNotification" object:self];
}

// Prints a message whenever a MyNotification is received
- (void)handleNotification:(NSNotification*)note {
	NSLog(@"Got notified: %@", note);
}

/**
 * Запрос к ВК
 */
-(NSDictionary*) requestToVk:(NSString*) urlString
{
	NSURL *url = [NSURL URLWithString:urlString];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	NSHTTPURLResponse *response = nil;
    NSError *error = nil;
	
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	NSDictionary *parsedData;
	
	if (responseData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
	} else {
		NSLog(@"Error with %@", error);
	}
	
	return parsedData;
}

-(void) testQuery
{
	NSString *fields = @"uid,first_name,last_name,screen_name,photo";
	NSString *name_case = @"nom";
	NSString *code = [NSString stringWithFormat:vkCheckNewMessagesWithUserInfo,fields,name_case,access_token];
	//NSNotificationCenter
	//@"messages:API.messages.get({\"access_token\":\"0aa3bd9401ef4eeb01ef4eeb4401de977b001ef01e65ee9512c76901d0d37998416a9f6\",\"filters\":\"1\",\"count\":\"10\"})";
	NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/execute?code=%@",[code stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ;
	NSMutableDictionary *response = [self requestToVk:urlString ];
	NSLog(@"%@",response);
	
	//return [self requestToVk:urlString ];
}

/**
 * Получение списка диалогов
 */
-(VkDialogs*) getDialogs:(int)limit from:(int)from {
	NSString *urlString = [NSString stringWithFormat:vkGetDialogs,access_token, @"1",countForCheckMsgs] ;
	NSLog(@"url string for get dialogs %@", urlString);
	NSDictionary *dialogsT = [self requestToVk:urlString];
	//NSLog(@"Dialogs %@", dialogsT);
	VkDialogs *dlg = [[VkDialogs alloc] initWithDictionary:dialogsT];
	return dlg;
}

/**
 * Загрузка и инициализация диалогов с vk.com
 */
-(void) loadDialogs{
	VkDialogs *dialogs = [self getDialogs:20 from:0];
	self.dialogs = dialogs;
}

/**
 * Return last countForCheckMsgs messages
 */
-(NSDictionary*) checkMessages
{
    NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/messages.get?access_token=%@&filters=%@&count=%@",access_token, @"1",countForCheckMsgs] ;
	
	return [self requestToVk:urlString ];
}


-(NSDictionary*) userByUid:(NSString*) uid fields:(NSString*) fields name_case:(NSString*) name_case
{
	NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?uid=%@&access_token=%@&fields=%@&name_case=%@",uid,access_token,fields,name_case] ;
	
	return [self requestToVk:urlString ];
}

-(void) postToWall 
{
    NSString* message = @"vkontakte+wall+posting";
    NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/wall.post?uid=%@&message=%@&attachments=http://google.com&access_token=%@", ID, message,access_token] ;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(responseString); // result
}

-(NSString*)access_token
{
	return access_token;
}

-(void) logout
{
	[userDefaults removeObjectForKey:@"vk_token"];
	[userDefaults synchronize];
}

-(NSDictionary*) getLongPullServer
{
	 return [[VkDelegate sharedInstance] requestToVk:[NSString stringWithFormat:@"https://api.vk.com/method/messages.getLongPollServer?access_token=%@",access_token]];
//	NSLog(@"%@",dict);
}


@end

//
//  VkMessage.m
//  VkMessager
//
//  Created by baistore on 02.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import "VkMessage.h"

@implementation VkMessage
@synthesize body = body_, uid = uid_, mid = mid_, title = title_, out_ = out__, date = date_;

-(id) init {
	self = [super init];
	return self;
}

-(void)setMessage:(NSDictionary *)msg{
	read_state = NO;
	self.body = [msg objectForKey:@"body"];
	self.date = [NSDate dateWithTimeIntervalSince1970:[[msg objectForKey:@"date"] doubleValue]];
	self.uid = [msg objectForKey:@"uid"];
	self.mid = [msg objectForKey:@"mid"];
	self.title = [msg objectForKey:@"title"];
}

-(void) setRead {
	read_state = YES;
}

-(BOOL) isReaded{
	return read_state;
}
@end


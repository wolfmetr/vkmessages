//
//  VkDialogs.h
//  VkMessages
//
//  Created by baistore on 20.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkDialog.h"
@interface VkDialogs : NSObject


@property (nonatomic, retain) NSMutableArray *dialogs;

-(NSUInteger) countDialogs;
-(void) addDialog:(VkDialog *) dlg;
-(NSMutableSet*) getUsersIdsSet;
-(id) initWithArray:(NSArray*) arr;
-(id) initWithDictionary:(NSDictionary*) dict;
-(void) initUsersFormVkUsers:(VkUsers*) dUsers;
@end

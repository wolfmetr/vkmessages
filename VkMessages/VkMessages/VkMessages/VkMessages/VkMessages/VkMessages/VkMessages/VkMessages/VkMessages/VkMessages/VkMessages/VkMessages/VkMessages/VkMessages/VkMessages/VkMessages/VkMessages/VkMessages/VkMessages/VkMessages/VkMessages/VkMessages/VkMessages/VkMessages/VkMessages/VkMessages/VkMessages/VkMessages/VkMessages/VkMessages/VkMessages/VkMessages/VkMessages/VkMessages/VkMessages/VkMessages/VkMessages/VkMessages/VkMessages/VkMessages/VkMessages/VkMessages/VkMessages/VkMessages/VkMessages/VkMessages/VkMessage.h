//
//  VkMessage.h
//  VkMessager
//
//  Created by baistore on 02.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VkMessage : NSObject{
	//@public
	BOOL read_state;
	@private
	NSString *body_;
	NSDate *date_; //дата отправки сообщения
	NSString *mid_; // id сообщения
	NSNumber *out__;
	
	NSString *title_; // заголовок сообщения или беседы
	NSString *uid_; // id автора
}
-(void) setMessage:(NSDictionary *)msg;
-(void) setRead;
-(BOOL) isReaded;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *mid;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSNumber *out_;
@end

//
//  DialogCellView.m
//  VkMessages
//
//  Created by baistore on 23.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "DialogCellView.h"
#import "VkDialog.h"
@interface DialogCellView ()

@end

@implementation DialogCellView
@synthesize dialog;

- (id)initWithDialog:(VkDialog*)dlg
{
    self = [super initWithNibName:@"DialogCellView" bundle:nil];
    if (self) {
        // Initialization code here.
		dialog = dlg;
    }
    
    return self;
}

/*
- (void)awakeFromNib // Инициализация полей после загрузки nib файла
{
	//[self.view addSubview:mainView];
	NSLog(@"DialogCellView awakeFromNib");
}*/

@end

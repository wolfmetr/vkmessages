//
//  main.m
//  VkMessages
//
//  Created by baistore on 17.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}

//
//  VkDialog.h
//  VkMessages
//
//  Created by baistore on 10.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkUsers.h"
@interface VkDialog : NSObject <NSCoding>{
	BOOL read_state;
	BOOL group;
	NSString *body;
	NSDate *date;
	NSString *mid;
	NSNumber *_out_;
	NSNumber *emoji_;
	NSString *title;
	NSString *uid;
	NSString *admin_id;
	NSString *chat_active;		// список uid пользователей как строка
	NSString *users_count;
	NSString *chat_id; 
	VkUsers	*users;
	NSMutableSet *usersIdsSet;	
}
-(void) setDialog:(NSDictionary *)dlg;
-(void) setRead;
-(BOOL) isReaded;
-(id) initWithDictionary:(NSDictionary *)dlg;
-(void) initUsersIdsSet;
-(void) initUsers:(VkUsers *) dUsers;

@property (nonatomic, assign) BOOL read_state;
@property (nonatomic, assign) BOOL group;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSDate *date;//дата отправки сообщения
@property (nonatomic, copy) NSString *mid;// id сообщения
@property (nonatomic, copy) NSNumber *out_;
@property (nonatomic, copy) NSNumber *emoji_;
@property (nonatomic, copy) NSString *title;// заголовок сообщения или беседы
@property (nonatomic, copy) NSString *uid;// id автора
@property (nonatomic, copy) NSString *admin_id; // id автора беседы
@property (nonatomic, copy) NSString *chat_active; // список участников беседы
@property (nonatomic, retain) VkUsers *users;
@property (nonatomic, copy) NSMutableSet *usersIdsSet; // хранится набор uid пользователей

@property (nonatomic, copy) NSString *users_count;
@property (nonatomic, copy) NSString *chat_id; //идентификатор беседы;


@end

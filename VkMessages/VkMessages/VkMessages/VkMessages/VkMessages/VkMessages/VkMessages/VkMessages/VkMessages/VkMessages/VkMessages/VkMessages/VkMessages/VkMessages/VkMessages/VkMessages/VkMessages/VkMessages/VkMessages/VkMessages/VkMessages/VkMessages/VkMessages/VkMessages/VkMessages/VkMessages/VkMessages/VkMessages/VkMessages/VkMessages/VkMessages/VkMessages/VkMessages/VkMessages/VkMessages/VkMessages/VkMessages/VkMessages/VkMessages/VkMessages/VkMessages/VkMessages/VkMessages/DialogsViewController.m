//
//  DialogsViewController.m
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "DialogsViewController.h"
#import "DialogCellView.h"
#import "DialogCell.h"
#import "VkDialog.h"
#import "VkDialogs.h"
@interface DialogsViewController ()

@end

@implementation DialogsViewController
@synthesize dialogs;

-(id)init{
	self = [super init];
    if (self) {
		
		//dialogs = [NSMutableArray array];
		dialogs = [[VkDialogs alloc] init];
		/*NSLog(@"fake init");
		NSDictionary *myDict = @{
							@"body" : @"Some text body",
							@"date" : @"1363883369",
							@"uid" : @"2314852",
							@"mid" : @"399928",
							@"title" : @"thats title"
		 };
		VkDialog *myDlg = [[VkDialog alloc] initWithDictionary:myDict];
		//DialogCellView *viewDlg = [[DialogCellView alloc] initWithDialog:myDlg];
		[dialogs addDialog:myDlg];
		
		NSDictionary *myDict1 = @{
							@"body" : @"Some text body12",
							@"date" : @"1363883333",
							@"uid" : @"2314853",
							@"mid" : @"399934",
							@"title" : @"thats title12"
	   };
		VkDialog *myDlg1 = [[VkDialog alloc] initWithDictionary:myDict1];
		//DialogCellView *viewDlg1 = [[DialogCellView alloc] initWithDialog:myDlg];
		[dialogs addDialog:myDlg1];*/
		//table1.s
		
	}
//	[table1 reloadData];
	
	return self;
}

- (void) resetDialogs:(VkDialogs*)dlgs
{
	[self.dialogs setDialogs:dlgs.dialogs];
	
}


//DialogCellView
- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	NSLog(@"numberOfRowsInTableView %lul",(unsigned long)dialogs.countDialogs);
	return dialogs.countDialogs;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)col row:(NSInteger)row
{
	NSTableCellView *result;
	DialogCell *cell = [tableView makeViewWithIdentifier:[DialogCell cellID] owner:self];
	if (!cell ) {
		cell = [DialogCell cell];
	}
	
	VkDialog* dlg = [dialogs.dialogs objectAtIndex:row];
	[cell.chatNameLabel setStringValue:dlg.title];
	[cell.lastMessageLabel setStringValue:dlg.body];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd-MM-yyyy"];
	[cell.timeLabel setStringValue:[dateFormatter stringFromDate:dlg.date]];
	if ([dlg.users.users count] > 0 ){
		NSLog(@"Dialog: %@ Image url: %@",dlg.title,[[dlg.users.users objectAtIndex:0] photo_100]);
		if ([[dlg.users.users objectAtIndex:0] photo_100])
			[cell.imageView downloadImageFromURL:[[dlg.users.users objectAtIndex:0] photo_100]];

	}
	result = cell;
	return result;
}

// Действие по выделению диалога
- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)rowIndex {
//	NSLog(@"%li tapped!", (long)rowIndex);
	
	/* 
	 * нужно определить id диалога
	 */
	VkDialog* dlg = [dialogs.dialogs objectAtIndex:rowIndex];
	NSLog(@"%@", dlg);
	return YES;
}

@end

//
//  DialogCell.m
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "DialogCell.h"

NSString *const kDialogCellIdentificator = @"DialogCell";

@implementation DialogCell

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(DialogCell*) cell {
	NSNib *cellNib = [[NSNib alloc] initWithNibNamed:@"DialogCellView" bundle:nil];
    NSArray *objects = nil;
    DialogCell* cell = nil;
    
    [cellNib instantiateNibWithOwner:nil topLevelObjects:&objects];
    for(id object in objects) {
        if([object isKindOfClass:[self class]]) {
            cell = object;
            break;
        }
    }

    
    return cell;
}

+(NSString*) cellID { return kDialogCellIdentificator; }

@end

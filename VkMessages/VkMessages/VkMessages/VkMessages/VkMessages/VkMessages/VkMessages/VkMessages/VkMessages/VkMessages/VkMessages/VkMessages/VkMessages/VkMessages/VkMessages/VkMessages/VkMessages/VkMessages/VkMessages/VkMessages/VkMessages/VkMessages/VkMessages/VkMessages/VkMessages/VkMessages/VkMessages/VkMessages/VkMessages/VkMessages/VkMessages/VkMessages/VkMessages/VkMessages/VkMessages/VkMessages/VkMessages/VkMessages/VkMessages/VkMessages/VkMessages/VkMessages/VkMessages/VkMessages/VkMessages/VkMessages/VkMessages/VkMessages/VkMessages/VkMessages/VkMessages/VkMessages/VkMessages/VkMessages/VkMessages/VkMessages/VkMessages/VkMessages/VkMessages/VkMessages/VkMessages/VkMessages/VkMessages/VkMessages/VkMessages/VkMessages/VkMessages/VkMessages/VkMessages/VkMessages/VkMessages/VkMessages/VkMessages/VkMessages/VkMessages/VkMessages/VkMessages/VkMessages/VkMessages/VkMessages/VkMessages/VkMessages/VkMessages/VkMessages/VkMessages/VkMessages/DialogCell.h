//
//  DialogCell.h
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PVAsyncImageView/PVAsyncImageView.h"

extern NSString *const kDialogCellIdentificator;
@interface DialogCell : NSTableCellView

//@property (retain, nonatomic) IBOutlet NSView *viewSelf;

@property (retain, nonatomic) IBOutlet PVAsyncImageView *imageView;
@property (retain, nonatomic) IBOutlet NSTextField *chatNameLabel;
@property (retain, nonatomic) IBOutlet NSTextField *lastMessageLabel;
@property (retain, nonatomic) IBOutlet NSTextField *timeLabel;


+(NSString*) cellID;
+(DialogCell*) cell;
@end

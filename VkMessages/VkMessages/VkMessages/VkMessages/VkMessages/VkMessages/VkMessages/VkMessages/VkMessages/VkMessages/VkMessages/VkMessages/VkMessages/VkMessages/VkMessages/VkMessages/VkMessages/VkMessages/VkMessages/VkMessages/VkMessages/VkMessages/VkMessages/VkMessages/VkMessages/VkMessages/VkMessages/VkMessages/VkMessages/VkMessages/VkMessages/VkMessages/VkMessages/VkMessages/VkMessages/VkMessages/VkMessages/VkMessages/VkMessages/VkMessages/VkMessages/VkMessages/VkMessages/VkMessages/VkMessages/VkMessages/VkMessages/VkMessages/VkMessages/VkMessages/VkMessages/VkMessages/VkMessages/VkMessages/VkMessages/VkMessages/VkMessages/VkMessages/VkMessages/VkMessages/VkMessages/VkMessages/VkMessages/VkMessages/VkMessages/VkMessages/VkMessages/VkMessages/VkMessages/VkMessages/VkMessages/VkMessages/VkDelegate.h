//
//  VkDelegate.h
//  VkMessager
//
//  Created by baistore on 02.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VkMessage.h"
#import "VkMessages.h"

#import	"VkDialog.h"
#import	"VkDialogs.h"


#define vkURLRequest1 @"https://oauth.vk.com/authorize?client_id=%@&scope=%@&redirect_uri=http://oauth.vk.com/blank.html&display=page&response_type=token"

// запрос возвращает последние 10 непрочитанных сообщений и 
#define vkCheckNewMessagesWithUserInfo @"return{messages:API.messages.get({\"filters\":\"1\",\"count\":\"10\"}),\
users:API.users.get({\"uids\":API.messages.get({\"filters\":\"1\",\"count\":\"10\"})@.uid,\"fields\":\"%@\",\"name_case\":\"%@\"})};&format=json&access_token=%@"
#define countForCheckMsgsDefault 10i;
#define vkGetDialogs @"https://api.vk.com/method/messages.getDialogs?access_token=%@&filters=%@&count=%@"
@interface VkDelegate : NSObject{
	NSNumber *countForCheckMsgs;
}

@property NSString *username, *realName, *ID, *link, *email, *access_token;
@property NSImage* photo;
@property (nonatomic, retain) VkMessages *messages;
@property (nonatomic, retain) VkDialogs *dialogs;

+ (id)sharedInstance;
-(NSString*)access_token;
-(BOOL) loginWithParams: (NSMutableDictionary*) params;
-(void) logout;
-(void) postToWall;
-(NSDictionary*) requestToVk:(NSString*) urlString;
-(NSDictionary*) checkMessages;
-(NSDictionary*) userByUid:(NSString*) uid fields:(NSString*) fields name_case:(NSString*) name_case;
-(VkDialogs*) getDialogs:(int)limit from:(int)from ;
-(void) testQuery;
-(void)notify;
- (void)handleNotification:(NSNotification*)note;
-(NSDictionary*) getLongPullServer;


@end

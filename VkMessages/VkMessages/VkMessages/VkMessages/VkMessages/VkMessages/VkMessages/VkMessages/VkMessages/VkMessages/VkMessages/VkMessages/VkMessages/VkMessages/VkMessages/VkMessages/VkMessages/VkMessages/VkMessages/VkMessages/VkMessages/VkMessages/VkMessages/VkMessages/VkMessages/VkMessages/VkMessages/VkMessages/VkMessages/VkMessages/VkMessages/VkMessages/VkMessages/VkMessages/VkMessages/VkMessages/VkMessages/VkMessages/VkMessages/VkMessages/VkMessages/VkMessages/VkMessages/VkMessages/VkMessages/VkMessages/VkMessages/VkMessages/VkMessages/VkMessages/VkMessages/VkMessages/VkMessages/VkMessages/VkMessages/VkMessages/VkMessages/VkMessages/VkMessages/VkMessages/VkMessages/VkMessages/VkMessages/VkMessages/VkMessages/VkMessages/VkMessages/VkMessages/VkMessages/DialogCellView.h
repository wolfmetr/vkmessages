//
//  DialogCellView.h
//  VkMessages
//
//  Created by baistore on 23.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class VkDialog;

@interface DialogCellView : NSViewController{
	VkDialog* dialog;
	NSView *mainView;
}

@property (readonly) VkDialog* dialog;
@property (strong) IBOutlet NSView *mainView;

- (id)initWithDialog:(VkDialog*)dlg;

@end

//
//  VkDialog.m
//  VkMessages
//
//  Created by baistore on 10.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkDialog.h"

@implementation VkDialog
@synthesize body,// = _body,
			uid,// = _uid,
			mid,// = _mid,
			title,// = _title,
			out_,// = _out,
			date,// = _date,
			emoji_,//= _emoji;
			chat_active,
			users,
			usersIdsSet,
			users_count,
			chat_id,
			group;

-(id) init {
	self = [super init];
	users = [[VkUsers alloc] init];
	usersIdsSet = [[NSMutableSet alloc] init];
	return self;
}

-(id) initWithDictionary:(NSDictionary *)dlg{
	self = [super init];
	if (self){
		self.read_state = NO;
		self.body = [dlg objectForKey:@"body"];
		self.date = [NSDate dateWithTimeIntervalSince1970:[[dlg objectForKey:@"date"] doubleValue]];
		self.uid = [dlg objectForKey:@"uid"];
		self.mid = [dlg objectForKey:@"mid"];
		self.title = [dlg objectForKey:@"title"];
		self.admin_id =([dlg objectForKey:@"admin_id"] != nil)?[dlg objectForKey:@"admin_id"]:nil;
		self.chat_active = ([dlg objectForKey:@"chat_active"] != nil)?[dlg objectForKey:@"chat_active"]:nil;
		self.chat_id = ([dlg objectForKey:@"chat_id"] != nil)?[dlg objectForKey:@"chat_id"]:nil;
		self.users_count = ([dlg objectForKey:@"users_count"] != nil)?[dlg objectForKey:@"users_count"]:nil;
		
		if (self.admin_id != nil) self.group = YES;
		[self initUsersIdsSet];
	}
	return self;
}

-(void)setDialog:(NSDictionary *)dlg{
	self.read_state = NO;
	self.body = [dlg objectForKey:@"body"];
	self.date = [NSDate dateWithTimeIntervalSince1970:[[dlg objectForKey:@"date"] doubleValue]];
	self.uid = [dlg objectForKey:@"uid"];
	self.mid = [dlg objectForKey:@"mid"];
	self.title = [dlg objectForKey:@"title"];
	self.admin_id =([dlg objectForKey:@"admin_id"] != nil)?[dlg objectForKey:@"admin_id"]:nil;
	self.chat_active = ([dlg objectForKey:@"chat_active"] != nil)?[dlg objectForKey:@"chat_active"]:nil;
	self.chat_id = ([dlg objectForKey:@"chat_id"] != nil)?[dlg objectForKey:@"chat_id"]:nil;
	self.users_count = ([dlg objectForKey:@"users_count"] != nil)?[dlg objectForKey:@"users_count"]:nil;
	
	if (self.admin_id != nil) self.group = YES;
	[self initUsersIdsSet];
}

// инициализация набора id пользователей
-(void) initUsersIdsSet
{
	if (self.admin_id){
		[self.usersIdsSet addObjectsFromArray:[[NSString stringWithFormat:@"%@",self.chat_active] componentsSeparatedByString:@","]];
	} else {
		[self.usersIdsSet addObject:self.uid];
	}
	
	
	NSLog(@"usersIdsSet : %@",[self usersIdsSet]);
}

-(void) setRead {
	self.read_state = YES;
}

-(BOOL) isReaded{
	return self.read_state;
}

- (NSString *)description
{	
	return [NSString stringWithFormat:@"\n body: %@ \n date: %@ \n mid: %@ \n out_: %@ \n emoji_: %@ \n title: %@ \n uid: %@ \n admin_id: %@ \n chat_active: %@", self.body, self.date, self.mid,self.out_,self.emoji_, self.title, self.uid, self.admin_id, self.chat_active];
}


-(void) initUsers:(VkUsers *) dUsers
{
	NSArray *usersArray = [NSArray arrayWithArray:[usersIdsSet allObjects]];
	
	for (NSString *uidUser in usersArray){
		[users addUser:[dUsers getUserByUid:uidUser]];
	}
	[self initUsersIdsSet];
	
}

#pragma mark - NSCoder protocol initialization

-(id)initWithCoder:(NSCoder *)decoder {
	self = [super init];
    if (self) {
        // Object
		self.body = [decoder decodeObjectForKey:@"body"];
		self.date = [decoder decodeObjectForKey:@"date"];
		self.mid = [decoder decodeObjectForKey:@"mid"];
		self.out_ = [decoder decodeObjectForKey:@"out"];
		self.emoji_ = [decoder decodeObjectForKey:@"emoji"];
		self.title = [decoder decodeObjectForKey:@"title"];
		self.uid = [decoder decodeObjectForKey:@"uid"];
    }
	return self;
}

-(void)encodeWithCoder:(NSCoder *)coder {
	
//	[super encodeWithCoder: coder];
	[coder encodeObject:body forKey:@"body"];
    [coder encodeObject:date forKey:@"date"];
    [coder encodeObject:mid forKey:@"mid"];
    [coder encodeObject:_out_ forKey:@"out"];
	[coder encodeObject:emoji_ forKey:@"emoji"];
	[coder encodeObject:title forKey:@"title"];
	[coder encodeObject:uid forKey:@"uid"];
}

@end

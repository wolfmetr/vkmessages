//
//  VkDialogs.m
//  VkMessages
//
//  Created by baistore on 20.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkDialogs.h"

@implementation VkDialogs
@synthesize dialogs;

-(id) init {
	self = [super init];
	if (self){
		dialogs = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id) initWithArray:(NSArray*) arr {
	self = [super init];
	if (self){
		dialogs = [[NSMutableArray alloc] initWithArray:arr];
	}
	return self;
}

-(id) initWithDictionary:(NSDictionary*) dict {
	self = [super init];
	if (self){
		int countNewDialogs = [[dict objectForKey:@"response"] count];
		NSMutableArray *result = [[NSMutableArray alloc] init];
		for (int i = 1; i < countNewDialogs; i++){
			VkDialog *dlg = [[VkDialog alloc] init];
			[dlg setDialog:[[dict objectForKey:@"response"] objectAtIndex:i]];
			[result addObject:dlg];
		}
		dialogs = [[NSMutableArray alloc] initWithArray:result];
	}
	return self;
}

-(NSUInteger) countDialogs
{
	return [dialogs count];
}

-(void) addDialog:(VkDialog *) dlg{
	[dialogs addObject:dlg];
}

-(NSMutableSet*) getUsersIdsSet
{
	NSMutableSet *userIds = [NSMutableSet set];
	for (VkDialog *dialog in dialogs){
		NSLog(@" dialog ids set: %@",[dialog usersIdsSet]);
		[userIds unionSet:[dialog usersIdsSet]];
		// если беседа, а не диалог
		/*if (dialog.chat_active != nil){
			// разбить chat_active на idшники и поместить в set
			[userIds addObjectsFromArray:[[dialog chat_active] componentsSeparatedByString: @","]];
		}*/
	}
	//[userIds minusSet:<#(NSSet *)#>];
	NSLog(@" user ids: %@",userIds);
	return userIds;
}

-(void) initUsersFormVkUsers:(VkUsers*) dUsers
{
	for (VkDialog *dialog in dialogs){
		[dialog initUsers:dUsers];
	}
}
@end

//
//  VkDataController.h
//  VkMessages
//
//  Created by baistore on 13.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkLongPollConnection.h"
#import "VkDelegate.h"
#import "DialogsViewController.h"
#import "DialogTable.h"

#import "VkChatViewController.h"
#import "ChatTable.h"

#import "VkUsers.h"
#import "VkDialogs.h"

#import "VkDialogsService.h"

#import "VkLongPollResponse.h"
#import "VkLongPollConnection.h"

#import "VkSendMessageService.h"

@interface VkDataController : NSObject <VkLongPollDelegateProtocol>

@property (nonatomic, strong) DialogsViewController	*dialogsViewController;
@property (nonatomic, strong) DialogTable			*dialogTable;

@property (nonatomic, strong) VkChatViewController	*chatViewController;
@property (nonatomic, strong) ChatTable				*chatTable;

@property (nonatomic, strong) VkLongPollConnection	*longPollService;
@property (nonatomic, strong) VkDialogsService		*dialogsService;
@property (nonatomic, strong) VkDialogsService		*dialogHistoryService;

@property (nonatomic, strong) VkUsers				*users;
@property (nonatomic, strong) VkDialogs				*dialogs;
@property (nonatomic, strong) VkDialogs				*messages;

@property (nonatomic, strong) VkSendMessageService	*sendMessageService;


//- (id) initWithDialogView:(DialogsViewController *)dViewController andTable:(DialogTable*)dTable;
+ (id)sharedInstance;
- (void) setDialogsView:(DialogsViewController *)dViewController withDialogTable:(DialogTable*)dTable andChatTable:(ChatTable*)cTable;
- (void) handleResponseWithUpdates:(VkLongPollResponse *)response;
- (void) updateDialogsList;
- (void) updateDialogHistoryByChatId:(NSString *)chatId;
- (void) updateDialogHistory:(NSString *)userID;

- (void) sendMessage:(NSString *)uid messageText:(NSString *)message isChat:(BOOL)is_chat;
@end

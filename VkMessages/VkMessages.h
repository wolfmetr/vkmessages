//
//  VkMessages.h
//  VkMessager
//
//  Created by baistore on 03.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkMessage.h"
@interface VkMessages : NSObject{
	NSMutableArray *messages_;
}

@property(nonatomic, retain) NSMutableArray *messages;

-(void) addMessage:(VkMessage *) msg;
-(NSUInteger) countMessages;
-(VkMessage*) messageAtIndex:(NSUInteger *) index;

@end

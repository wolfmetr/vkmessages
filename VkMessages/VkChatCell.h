//
//  VkChatCell.h
//  VkMessages
//
//  Created by baistore on 07.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PVAsyncImageView/PVAsyncImageView.h"

extern NSString *const kVkChatCellIdentificator;
@interface VkChatCell : NSTableCellView


@property (weak) IBOutlet PVAsyncImageView *imageView;
@property (weak) IBOutlet NSTextField *lastMessageLabel;
@property (weak) IBOutlet NSTextField *timeLabel;
@property (weak) IBOutlet NSTextField *nameField;

+(NSString*) cellID;
+(VkChatCell*) cell;
@end

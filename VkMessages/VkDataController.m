//
//  VkDataController.m
//  VkMessages
//
//  Created by baistore on 13.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkDataController.h"

@implementation VkDataController
@synthesize dialogs,
			messages,
			users,
			longPollService,
			dialogTable,
			dialogsViewController,
			chatTable,
			chatViewController;

// класс синглтон
+ (id)sharedInstance {
    static VkDataController *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[VkDataController alloc]init];
    });
    return __sharedInstance;
}

- (id) init
{
	self = [super init];
	if (self){
		users = [[VkUsers alloc] init];
		dialogs = [[VkDialogs alloc] init] ;
		messages = [[VkDialogs alloc] init] ;
		
		longPollService = [[VkLongPollConnection alloc] initWithDelegate:self];
		[longPollService startPoll];
	}
	return self;
}

// Инициализация отображения диалога
-(void) setDialogsView:(DialogsViewController *)dViewController withDialogTable:(DialogTable*)dTable andChatTable:(ChatTable*)cTable
{
	self.dialogTable = dTable;
	self.dialogsViewController = dViewController;
	self.chatTable = cTable;
	if (!self.dialogsViewController){
		self.dialogsViewController = (DialogsViewController*)self.dialogTable.delegate;
		self.dialogsViewController.chatViewController = (VkChatViewController*)self.chatTable.delegate;
	}

	// получаю список диалогов
	[self updateDialogsList];
}

/*
 Обновить список диалогов (200 штук)
 */
- (void)updateDialogsList
{
    __weak VkDataController *weakSelf = self;
    void (^completionBlock)(VkDialogs *) = ^(VkDialogs *dialogs) {
		NSLog(@"VKDataController dialogs completion block: %ld", (unsigned long)[dialogs countDialogs]);
		[[weakSelf dialogs] setDialogs:[dialogs dialogs]];
		// получить id юзеров из диалогов
		NSMutableSet *userIdsSet = weakSelf.dialogs.getUsersIdsSet;
		[weakSelf.users addUsersFromSetIds:userIdsSet];
		[dialogs initUsersFormVkUsers:weakSelf.users];
		
		
		[[weakSelf.dialogsViewController dialogs] setDialogs:[weakSelf.dialogs dialogs]];
		// Обновить таблицу диалогов
		[weakSelf.dialogTable reloadData];
    };
	
    if (!self.dialogsService)
        self.dialogsService = [[VkDialogsService alloc] initWithCompletionBlock:completionBlock];
    [self.dialogsService getDialogs];
}

/*
 Получить сообщения из беседы с пользователем userID (200 шт)
 */
- (void)updateDialogHistory:(NSString *)userID
{
    __weak VkDataController *weakSelf = self;
    void (^completionBlock)(VkDialogs *) = ^(VkDialogs *dialogs) {
		[[weakSelf messages] setDialogs:[dialogs dialogs]];
		
		// получить id юзеров из диалогов
		NSMutableSet *userIdsSet = weakSelf.dialogs.getUsersIdsSet;
		[weakSelf.users addUsersFromSetIds:userIdsSet];
		[dialogs initUsersFormVkUsers:weakSelf.users];
		
		
		// добавить список сообщений
		[weakSelf.dialogsViewController.chatViewController resetChat:weakSelf.messages];
		// обновить таблицу
		[weakSelf.chatTable reloadData];
		// промотать до последнего
		[weakSelf.dialogsViewController scrollToBottom];
    };
	
    if (!self.dialogHistoryService)
        self.dialogHistoryService = [[VkDialogsService alloc] initWithCompletionBlock:completionBlock];
    [self.dialogHistoryService getDialogHistoryByUid:userID];
}

/*
 Получить сообщения из беседы по chat_id = chatId (200 шт)
 */
- (void)updateDialogHistoryByChatId:(NSString *)chatId
{
    __weak VkDataController *weakSelf = self;
    void (^completionBlock)(VkDialogs *) = ^(VkDialogs *dialogs) {
		[[weakSelf messages] setDialogs:[dialogs dialogs]];
		
		// получить id юзеров из диалогов
		NSMutableSet *userIdsSet = weakSelf.messages.getUsersIdsSet;
		[weakSelf.users addUsersFromSetIds:userIdsSet];
		[dialogs initUsersFormVkUsers:weakSelf.users];
		
		// добавить список сообщений
		[weakSelf.dialogsViewController.chatViewController resetChat:weakSelf.messages];
		// обновить таблицу
		[weakSelf.chatTable reloadData];
		// промотать до последнего
		[weakSelf.dialogsViewController scrollToBottom];
    };
	
    if (!self.dialogHistoryService)
        self.dialogHistoryService = [[VkDialogsService alloc] initWithCompletionBlock:completionBlock];
    [self.dialogHistoryService getDialogHistoryByChatId:chatId];
}

#pragma mark - VkLongPollDelegateProtocol
/*
	Process long poll data
 */
- (void)handleResponseWithUpdates:(VkLongPollResponse *)response
{
//	NSArray *updates = [NSArray arrayWithArray:response.updates];
	NSLog(@"handleResponseWithUpdates with array %@", response.updates);
	NSMutableArray *updateMsgsIds = [[NSMutableArray alloc] init];
	for (NSArray *curUpdate in response.updates) {
        unsigned eventType = ((NSNumber *) [curUpdate objectAtIndex:0]).unsignedIntValue;
        
		NSLog(@"\tmessage event [code: %d]", eventType);
        switch (eventType) {
			case 0: {
				//0,$message_id,0 -- удаление сообщения с указанным local_id
				break;
			}
            case 1: {
				// 1,$message_id,$flags -- замена флагов сообщения (FLAGS:=$flags)
				break;
			}
            case 2: {
				// 2,$message_id,$mask[,$user_id] -- установка флагов сообщения (FLAGS|=$mask)
				break;
			}
            case 3: {
				// 3,$message_id,$mask[,$user_id] -- сброс флагов сообщения (FLAGS&=~$mask)
				break;
			}
            case 4: {
				//4,$message_id,$flags,$from_id,$timestamp,$subject,$text,$attachments -- добавление нового сообщения
				[updateMsgsIds addObject:(NSString*)[curUpdate objectAtIndex:1]];
                break;
            }
            case 8: {
				// 8,-$user_id,0 -- друг $user_id стал онлайн
				NSInteger userId = -1 * ((NSNumber *) [curUpdate objectAtIndex:1]).integerValue;
          //      [self changeUserStatus:userId online:YES];
                break;
            }
            case 9: {
				// 9,-$user_id,$flags -- друг $user_id стал оффлайн ($flags равен 0, если пользователь покинул сайт (например, нажал выход) и 1, если оффлайн по таймауту (например, статус away))
				NSInteger userId = -1 * ((NSNumber *) [curUpdate objectAtIndex:1]).integerValue;
          //      [self changeUserStatus:userId online:NO];
                break;
            }
			case 51: {
				//51,$chat_id,$self -- один из параметров (состав, тема) беседы $chat_id были изменены. $self - были ли изменения вызываны самим пользователем
				break;
			}
			case 61: {
				// 61,$user_id,$flags -- пользователь $user_id начал набирать текст в диалоге. событие должно приходить раз в ~5 секунд при постоянном наборе текста. $flags = 1
				NSInteger userId = -1 * ((NSNumber *) [curUpdate objectAtIndex:1]).integerValue;
				break;
			}
			case 62: {
				// 62,$user_id,$chat_id -- пользователь $user_id начал набирать текст в беседе $chat_id.
				NSInteger userId = -1 * ((NSNumber *) [curUpdate objectAtIndex:1]).integerValue;
				break;
			}
			case 70: {
				// 70,$user_id,$call_id -- пользователь $user_id совершил звонок имеющий идентификатор $call_id, дополнительную информацию о звонке можно получить используя метод voip.getCallInfo.
				NSInteger userId = -1 * ((NSNumber *) [curUpdate objectAtIndex:1]).integerValue;
				break;
			}
				
            default:
                NSLog(@"\tunhandled event [code: %d]", eventType);
        }
    }
	
	NSLog(@"Update MESSAGES = %@", updateMsgsIds);
	
}

- (void) sendMessage:(NSString *)uid messageText:(NSString *)message isChat:(BOOL)is_chat
{
	__weak VkDataController *weakSelf = self;
    void (^completionBlock)(NSString *) = ^(NSString *mid) {
		NSLog(@"Message has been send. mid = %@", mid);
    };
	
    if (!self.sendMessageService)
        self.sendMessageService = [[VkSendMessageService alloc] initWithCompletionBlock:completionBlock];
    [self.sendMessageService sendMessage:uid messageText:message isChat:is_chat];
}
@end

//
//  VkChatCell.m
//  VkMessages
//
//  Created by baistore on 07.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkChatCell.h"

NSString *const kVkChatCellIdentificator = @"VkChatCell";
@implementation VkChatCell

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}



+(VkChatCell*) cell {
	NSNib *cellNib = [[NSNib alloc] initWithNibNamed:@"ChatCellView" bundle:nil];
    NSArray *objects = nil;
    VkChatCell* cell = nil;
    
    [cellNib instantiateNibWithOwner:nil topLevelObjects:&objects];
    for(id object in objects) {
        if([object isKindOfClass:[self class]]) {
            cell = object;
            break;
        }
    }
    
    return cell;
}

+(NSString*) cellID { return kVkChatCellIdentificator; }


@end

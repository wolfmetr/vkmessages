//
//  VkDialogsService.h
//  VkMessages
//
//  Created by baistore on 19.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"
#import "VkDialogs.h"

@interface VkDialogsService : NSObject

@property(nonatomic, copy) void (^completionBlock)(VkDialogs *);
//@property(nonatomic, strong) RKClient *rkClient;

-(id) initWithCompletionBlock:(void (^)(VkDialogs *))arg;
-(void) getDialogs;

-(void) getDialogHistoryByUid:(NSString *)userId;
-(void) getDialogHistoryByChatId:(NSString *)chatId;
-(void) getDialogHistoryBy:(NSString *)typeGet withParam:(NSString *) param;
//-(BOOL) isLoading;


@end

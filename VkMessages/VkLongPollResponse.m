//
//  VkLongPollResponse.m
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VKLongPollResponse.h"
#import "RestKit/RestKit.h"


@implementation VkLongPollResponse

+ (RKObjectMapping *)mapping {
	[RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/javascript"];
    RKObjectMapping *longPollMapping = [RKObjectMapping mappingForClass:[self class]];
	[longPollMapping addAttributeMappingsFromDictionary:@{
	 @"ts":			@"ts",
	 @"updates":	@"updates",
	 @"failed":		@"failed"
	 }];
    return longPollMapping;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\n updates: %@ \n ts: %@ ", self.updates, self.ts];
}

@end
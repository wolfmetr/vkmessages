//
//  VkSendMessageService.m
//  VkMessages
//
//  Created by baistore on 21.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkSendMessageService.h"

@implementation VkSendMessageService


- (id)initWithCompletionBlock:(void (^)(NSString *))arg
{
    self = [super init];
    if (self) {
        self.completionBlock = arg;
    }
    return self;
}


- (void) sendMessage:(NSString *)uid messageText:(NSString *)message isChat:(BOOL)is_chat
{
	NSString *tokenParam = [[NSUserDefaults standardUserDefaults] valueForKey:@"vk_token"];
    NSString *path = @"/method/messages.send";
	NSString *param = @"uids";
	NSString *timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];

	if (is_chat)
		param = @"chat_id";

	NSDictionary *paramsRequest = @{
		@"access_token": tokenParam,
		param : uid,
		@"message" : message,
		@"guid": timeStamp //уникальный идентификатор, предназначенный для предотвращения повторной отправки одинакового сообщения.
	};
	
	RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://api.vk.com"]];
	[[objectManager HTTPClient] getPath:path
							 parameters:paramsRequest
								success:^(AFHTTPRequestOperation *operation, id responseObject)
	 {

		 NSLog(@"SendMessage result %@",[responseObject objectForKey:@"response"]);

		 NSString *result = (NSString*) [responseObject objectForKey:@"response"] ;
		 if (_completionBlock)
			 _completionBlock(result);
	 }
								failure:^(AFHTTPRequestOperation *operation, NSError *error)
	 {
		 NSLog(@"SendMessage failure %@",error);
	 }];
}
@end

//
//  vkAppDelegate.h
//  VkMessages
//
//  Created by baistore on 17.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

#import "VkDelegate.h"
#import "VkMessage.h"
#import "VkMessages.h"
#import "VkDialog.h"
#import "VkDialogs.h"
#import "VkUser.h"
#import "VkUsers.h"
#import "VkLongPollConnection.h"
#import "VkDataController.h"
#import "Menu.h"
#import "DialogTable.h"
#import "ChatTable.h"

#define vkAPI_ID @"3266960"
#define vkRules @"offline,friends,messages,notifications"

@class PreferencesWindowController, DialogsViewController, VkChatViewController;

@interface vkAppDelegate : NSObject <NSApplicationDelegate,NSUserNotificationCenterDelegate>{
	//Preferences panel
	PreferencesWindowController *preferences;
	DialogsViewController *dialogsViewController;
	VkChatViewController *chatViewController;
	
	VkDataController *dataController;
	
	VkLongPollConnection *lpConnectionHandler;
	
	int updateTime;
	NSInteger *countNewMessages;
	VkMessages *messages_;
	
	NSString *server;
	NSString *key;
	NSString *ts;
	
	NSUserNotificationCenter *center;
	NSMutableArray *showedMessages_;
	NSDockTile *dockTile;
	NSOperationQueue *queueTimerUpdate; // поток для подгрузки сообщений
	
	//DialogsViewController* dialogDelegate;
	
	IBOutlet NSView *authView;
	//IBOutlet NSTableView *dialogTable;
	IBOutlet WebView *authWebView;
	IBOutlet NSProgressIndicator *progress;
	
	IBOutlet DialogTable *dialogTable;
	IBOutlet ChatTable *chatTable;
}

//@property (weak) IBOutlet NSScrollView *scrollView;
//@property (nonatomic, retain) DialogsViewController *dialogDelegate;
@property (nonatomic, retain) VkMessages *messages;
@property (nonatomic, retain) VkDialogs	*dialogs;
@property (nonatomic, retain) VkUsers *users;
@property (assign) IBOutlet DialogTable *dialogTable;
@property (assign) IBOutlet ChatTable *chatTable;

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSMenu *trayMenu;
@property (strong) Menu *men;
@property (weak) NSTimer *myTimer;
//@property (assign) NSTableView *dialogTable;

- (IBAction)send:(id)sender;
- (void) userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification;
- (void) webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame;
- (void) checkMessage;
- (void) sendNotification: (NSString *) msg author:(NSString *) name;
- (void) initDataOnSuccessLogin;

//Long pull actions
- (void) longPollWrapper:(NSDictionary *)args; 
- (void) longPoll:(NSString*)server withKey:(NSString*)key andTimestamp:(NSString*) ts;
- (void) startPoll;
- (void) dataReceived: (NSData*) theData;

- (void) testBlock:(NSString*) msg;

// Chat
- (void) openDialogWithMid:(NSString *) midDialog;

//Preferences panel
- (IBAction)showPreferencesPanel:(id)sender;

// Menu
- (IBAction)toSite:(id)sender;
@end

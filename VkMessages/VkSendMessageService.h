//
//  VkSendMessageService.h
//  VkMessages
//
//  Created by baistore on 21.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface VkSendMessageService : NSObject
@property(nonatomic, copy) void (^completionBlock)(NSString *);
- (void) sendMessage:(NSString *)uid messageText:(NSString *)message isChat:(BOOL)is_chat;
- (id) initWithCompletionBlock:(void (^)(NSString *))arg;
@end

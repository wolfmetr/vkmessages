//
//  VkUser.m
//  VkMessages
//
//  Created by baistore on 06.03.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkUser.h"
#import "vkAppDelegate.h"
@implementation VkUser

-(id) init{
	if (self = [super init]){
//		[[self firstName] initWithString:@"unknown"];
//		[[self lastName] initWithString:@"unknown"];
//		[[self screenName] initWithString:@"unknown"];
//		[[self nickname] initWithString:@"unknown"];
//		[[self uid] initWithString:@"0"];
//		[[self photo] init];
	}
	return self;
}


+(NSArray*) loadByUid:(NSString *) uid{
	NSString *access_token = [[VkDelegate sharedInstance] access_token];
	NSString *fields = @"uid,first_name,last_name,screen_name,photo,photo_100,nickname";
	NSString *name_case = @"nom";
	NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?uids=%@&access_token=%@&fields=%@&name_case=%@",uid,access_token,fields,name_case] ;
	//NSMutableDictionary *user_data = [[(NSMutableDictionary *)[[VkDelegate sharedInstance] requestToVk:urlString] objectForKey:@"response"] objectAtIndex:0];
	
	NSArray *user_data = [(NSMutableDictionary *)[[VkDelegate sharedInstance] requestToVk:urlString] objectForKey:@"response"];
	//NSLog(@"loadByUid user_data: %@",user_data);
	return user_data;
}

+(id) initByUid:(NSString *) uid
{
	//NSLog(@"dict user with uid %@: %@", uid,user_data);
	
	return [VkUser initByDictionary:[[VkUser loadByUid:uid] objectAtIndex:0]];
}

+(id) initByDictionary:(NSDictionary *) dictionary
{
	VkUser *user = [[VkUser alloc] init];
//	NSLog(@"USER---- %@",dictionary);
	user.firstName =  [dictionary objectForKey:@"first_name"];
	user.lastName =  [dictionary objectForKey:@"last_name"];
	user.nickname =  [dictionary objectForKey:@"nickname"];
	user.screenName =  [dictionary objectForKey:@"screen_name"];
	user.uid =  [dictionary objectForKey:@"uid"];
	user.photo_100 =  [dictionary objectForKey:@"photo_100"];
	
//	NSLog(@"Init user %@ %@", user.firstName, user.lastName);
//	NSLog(@"User photo %@", user.photo_100);
	return user;
}

+(id) initByUser:(VkUser *) user
{
	VkUser *userRes = [[VkUser alloc] init];
	
	userRes.firstName = user.firstName;
	userRes.lastName = user.lastName;
	userRes.nickname = user.nickname;
	userRes.screenName = user.screenName;
	userRes.uid = user.uid;
	userRes.photo_100 = user.photo_100;
	
	return user;
}


-(void) setPhoto:(NSImage *)photo withURL:(NSString *)url
{
	// TODO: загружать асинхронно
	//[[self photo] initWithContentsOfURL:[NSURL URLWithString:url]];
}

- (NSString *)description
{
	return [NSString stringWithFormat: @"\n uid: %@ \n firstName: %@ \n lastName: %@", self.uid, self.firstName, self.lastName];
}


@end

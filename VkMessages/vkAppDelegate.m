//
//  vkAppDelegate.m
//  VkMessages
//
//  Created by baistore on 17.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import "vkAppDelegate.h"
#import "VkMessage.h"
#import "PreferencesWindowController.h"
#import "DialogsViewController.h"
#import <WebKit/WebKit.h>
#import "VkUser.h"

#import "VkDialogsService.h"

@implementation vkAppDelegate
@synthesize messages = messages_;
@synthesize dialogTable = _dialogTable;
//@synthesize dialogTable;

-(id)init{
	self = [super init];
    if (self) {
		updateTime = [[[NSUserDefaults standardUserDefaults] objectForKey:VKCUpdateTimeKey] intValue];
		// вешаем слушателя на изменение времени для обновления
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self
			   selector:@selector(handleUpdateTimeChange:)
				   name:VKUpdateTimeChangedNotification
				 object:nil];
		_users = [[VkUsers alloc] init];
		_dialogs = [[VkDialogs alloc] init] ;
		
		/*lpConnectionHandler = [[VkLongPullConnection alloc] initWithCompletionBlock:^(NSObject *object) {
			[self testBlock:@"Is test block"];
		}];*/
//		[[VkLongPullConnection alloc] initWithDelegate:self];
		
		chatViewController = (VkChatViewController *)chatTable.delegate;
		dialogsViewController = (DialogsViewController*)dialogTable.delegate;
		dialogsViewController.chatViewController = chatViewController;
		_chatTable.reloadData;
		//dialogsViewController.chatViewController = [[VkChatViewController alloc] init];
		
		//dataController = [[VkDataController alloc] init];
//		dataController = [[VkDataController alloc] initWithDialogView:dialogsViewController andTable:_dialogTable];
		//lpConnectionHandler = [[VkLongPollConnection alloc] initWithDelegate:dataController];
		
	}
	return self;
}
+ (void)initialize
{

}

- (void)handleUpdateTimeChange:(NSNotification *)note
{
	NSLog(@"Received notification: %@", note);
	NSNumber *time = [[note userInfo] objectForKey:@"time"];
	updateTime = [time intValue];
//	NSLog(@"Received notification:tr");
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	_men = [[Menu alloc] initTrayWithMenu:_trayMenu]; // Initializating (I've write custom init method) status bar icon w/ menu
	center = [NSUserNotificationCenter defaultUserNotificationCenter];
	[center setDelegate:self];
	dockTile = [NSApp dockTile];
	
	authWebView = [[WebView alloc] initWithFrame:authView.frame];
	[authView addSubview:authWebView];
	[authWebView setFrameLoadDelegate:self];
	NSString *urlAddress = [NSString stringWithFormat:vkURLRequest1,vkAPI_ID,vkRules];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [[authWebView mainFrame] loadRequest:requestObj];
	
	self.messages = [[VkMessages alloc] init];
	queueTimerUpdate = [[NSOperationQueue alloc] init];
}

/**************************
 Получение новых сообщений
 **************************/
- (void)checkMessage
{
	//	получаю список последних 10 диалогов
	self.dialogs = [[VkDelegate sharedInstance] getDialogs:10 from:1];
	
	// получить id юзеров из диалогов
	NSMutableSet *userIdsSet = self.dialogs.getUsersIdsSet;

	
	[self.users addUsersFromSetIds:userIdsSet];
	NSLog(@"ALL USERS: %@", self.users.users);
	
	[self.dialogs initUsersFormVkUsers:self.users];
	if (!dialogsViewController){
		dialogsViewController = (DialogsViewController*)_dialogTable.delegate;
		dialogsViewController.chatViewController = _chatTable.delegate;
	}
		
	[[dialogsViewController dialogs] setDialogs:[self.dialogs dialogs]];
	[_dialogTable reloadData];
	
	// получаю словарь сообщений с сервера
	NSMutableDictionary *messagesNew = (NSMutableDictionary *)[[VkDelegate sharedInstance] checkMessages];
	// количество сообщений
	countNewMessages = (NSInteger *)[[[messagesNew objectForKey:@"response"] objectAtIndex:0] integerValue];

	NSNumber *cnt = [NSNumber numberWithInteger:countNewMessages];
	// пересчитываем бейдж на иконке снизу
	[_men setMessage: [NSString stringWithFormat:@"%@",cnt]];
	if (cnt.intValue > 0)
		[dockTile setBadgeLabel:[NSString stringWithFormat:@"%@",cnt]];
	else
		[dockTile setBadgeLabel:@""];

	// заполнение класса хранилища сообщений
	for (int i = 1; i <= countNewMessages; i++){
		VkMessage *msg = [[VkMessage alloc] init];
		[msg setMessage:[[messagesNew objectForKey:@"response"] objectAtIndex:i]];
		BOOL contain = NO;
		// проверяем есть ли сообщение в списке
		for (VkMessage *m in [self.messages messages]){
			if ([[m mid] isEqual:msg.mid]){
				contain = YES;
				break;
			}	
		}
		// если сообщения нет, то добавляем
		if (!contain)
			[self.messages addMessage:msg];
	}
	
	
	/*
	 Для каждого сообщения получаем информацию
	 о его авторе и отправлем уведомление в UserNotificationCenter
	 */
	for (VkMessage *m in [self.messages messages]){
		VkUser *curUser;
		// если пользователя с таким uid ещё нет в словаре
		if (![self.users getUserByUid:m.uid])
		{
			// инициализируем пользователя
			curUser = [VkUser initByUid:m.uid];
			// добавляем пользователья в словарь
			[self.users addUser:curUser];
			NSLog(@"count users %lu", (unsigned long)[self.users countUsers]);
		} else {
			curUser = [self.users getUserByUid:m.uid];
		}
		
		NSLog(@"curUser = %@", [self.users getUserByUid:[m uid]]);
		//NSLog(@"self.users = %@", self.users.users);
		
		if (![m isReaded]){
			// отправляем сообщение в UserNotificationCenter
			[self sendNotification:[NSString stringWithFormat:@"%@",m.body ]
							author:[NSString stringWithFormat:@"%@",[[curUser.firstName stringByAppendingString:@" "] stringByAppendingString:curUser.lastName]]];
			[m setRead];
		}
	}
}




- (IBAction)send:(id)sender
{

	/*NSPoint newScrollOrigin;
	
    // assume that the scrollview is an existing variable
    if ([[self.scrollView documentView] isFlipped]) {
        newScrollOrigin=NSMakePoint(0.0,NSMaxY([[self.scrollView documentView] frame])
									-NSHeight([[self.scrollView contentView] bounds]));
    } else {
        newScrollOrigin=NSMakePoint(0.0,0.0);
    }
	
    [[self.scrollView documentView] scrollPoint:newScrollOrigin];
	 */
	//updateDialogsList
	//[[VkDataController sharedInstance] updateDialogsList];
//	[[VkDataController sharedInstance] updateDialogHistoryByChatId:@"31"];
	//VkDialogsService *vkDialogsService = [[VkDialogsService alloc] init];
	//[vkDialogsService getDialogs];
	//[lpConnectionHandler startPoll];

}

- (void) openDialogWithMid:(NSString *) midDialog
{
	NSLog(@"openDialogWithMid : %@", midDialog);
}

#pragma mark - WebView delegate
- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	//NSLog(@"WebView didFinishLoadForFrame");
	NSMutableDictionary* user = [[NSMutableDictionary alloc] init];
	NSString *currentURL = frame.webView.mainFrameURL; // получаем текущий URL
	NSRange textRange =[[currentURL lowercaseString] rangeOfString:[@"access_token" lowercaseString]];
	if(textRange.location != NSNotFound){
		NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];	
        [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
        [user setObject:[data objectAtIndex:3] forKey:@"expires_in"];
        [user setObject:[data objectAtIndex:5] forKey:@"user_id"];

		if ([[VkDelegate sharedInstance] loginWithParams:user]){
			[authWebView removeFromSuperview];
			//[_window close];
			[self initDataOnSuccessLogin];
		} else {
			[authWebView removeFromSuperview];
		}
	} else {
		NSLog(@"VK: Access denied");
	}
}

-(void) initDataOnSuccessLogin
{
	[[VkDataController sharedInstance] setDialogsView:dialogsViewController withDialogTable:self.dialogTable andChatTable:self.chatTable];
	//lpConnectionHandler = [[VkLongPollConnection alloc] initWithDelegate:dataController];
}

#pragma mark - UserNotifications delegate
- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
	return YES;
}


/**
 * Action requires when user click to notification message
 */
- (void) userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification
{
	if ([notification activationType] == NSUserNotificationActivationTypeContentsClicked){
		[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.vk.com/im"]];
	}
	
}

/**
 *	Send new user-notification to system
 */
- (void) sendNotification: (NSString *) msg author:(NSString *) name
{
	NSUserNotification *notification = [[NSUserNotification alloc] init];
	//[notification setTitle:@"Новое сообщение VK"];
	[notification setTitle:name];
	//[notification setSubtitle:name];
	if ([msg isEqual:@""]) msg = @"Медиа контент";
	[notification setInformativeText:msg];
	[notification setDeliveryDate:[NSDate dateWithTimeInterval:5 sinceDate:[NSDate date]]];
	[notification setSoundName:NSUserNotificationDefaultSoundName];
	[center scheduleNotification:notification];
}

#pragma mark - Preferences
- (IBAction)showPreferencesPanel:(id)sender
{
	if(!preferences){
		preferences = [[PreferencesWindowController alloc] init];
	}
	[preferences showWindow:self];
	
}

#pragma mark - Menu
- (IBAction)toSite:(id)sender
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.vk.com/im"]];
}




@end

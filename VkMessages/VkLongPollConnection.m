//
//  VkLongPullConnection.m
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkLongPollConnection.h"

@implementation VkLongPollConnection
@synthesize server,key,ts;

- (id)initWithDelegate:(id <VkLongPollDelegateProtocol>)delegate
{
	self = [super init];
	if (self){
		self.breakFlag = NO;
		self.delegate = delegate;
	}
	return self;
}

- (void) startPoll
{
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[VkLongPollInfo mapping] pathPattern:nil keyPath:@"response" statusCodes:nil];
	
	NSString *token = [[VkDelegate sharedInstance] access_token];
    NSString *resourcePath = [NSString stringWithFormat:@"https://api.vk.com/method/messages.getLongPollServer?access_token=%@", token];
	NSURL *url = [NSURL URLWithString:resourcePath];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	
	RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request
																		responseDescriptors:@[responseDescriptor]];
	
	
	[operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
		VkLongPollInfo *res = [[result array] objectAtIndex:0];
		self.key = [res key];
		self.server = [res server];
		self.ts = [res ts];
		[self longPoll];
		
	} failure:nil];
	
	[operation start];
}

-(void) longPoll
{
	NSLog(@"====LongPoll start with ts = %@",ts);
	RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[VkLongPollResponse mapping] pathPattern:nil keyPath:nil statusCodes:nil];
    NSString *resourcePath = [NSString stringWithFormat:@"http://%@?act=a_check&key=%@&ts=%@&wait=25&mode=0",server, key, ts];

	NSURL *url = [NSURL URLWithString:resourcePath];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	
	RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request
																		responseDescriptors:@[responseDescriptor]];
	
	__weak VkLongPollConnection *weakSelf = self;
	[operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
		VkLongPollResponse *responseObject = [[result array] objectAtIndex:0];
		
		NSString *oldTs = [NSString stringWithString:self.ts];
		NSLog(@"BEFORE %d",[oldTs intValue]);
		NSInteger myInt = (int)[oldTs intValue]-1 ;
		NSLog(@"AFTER %ld",(long)myInt);
		oldTs = [NSString stringWithFormat:@"%ld", (long)myInt];
		
		NSLog(@"===oldTS = %@",oldTs);
		weakSelf.ts = responseObject.ts;
		NSLog(@"NEW TS = %@",responseObject.ts);
		
		if (responseObject.failed) {
			NSLog(@"long poll failed!");
			[weakSelf restart];
			return;
		}
		
		if (responseObject.updates.count) {
			NSLog(@"ts: %@, updates count: %ld", responseObject.ts, (unsigned long)responseObject.updates.count);
			[weakSelf.delegate handleResponseWithUpdates:(id)responseObject];
			NSLog(@"====Start longPollHistory with oldTS = %@",oldTs);
			[weakSelf longPollHistory:oldTs];
		}
		
		if (!weakSelf.breakFlag){
			[weakSelf performSelectorInBackground:@selector(longPoll)
			 					   withObject: nil];
		}
			
		
		NSLog(@"Count responseObject.updates.count = %lu", (unsigned long)responseObject.updates.count);
		return;
		
	} failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Operation failed with error: %@", error);
		[weakSelf restart];
		return;
    }];
	
	NSLog(@"+===========START OPERATION===========");
	[operation start];
}


- (void) restart
{
	[self startPoll];
}

- (void) dataReceived: (NSData*) theData
{
	NSLog(@"process the response here");
	
	NSError *error = nil;
	NSDictionary *parsedData;
	
	if (theData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:theData
													 options:kNilOptions
													   error:&error];
		NSLog(@"%@",parsedData);
		if (self.delegate){
			NSArray *res = [[NSArray alloc] init];
			[self.delegate handleResponseWithUpdates:res];
		}
	} else {
		NSLog(@"Error with %@", error);
	}
}



- (void) longPollHistory:(NSString *)newTs
{
	NSLog(@"longPollHistory");
	
    //compose the request
    NSError* error = nil;
    NSURLResponse* response = nil;
	NSString *token = [[VkDelegate sharedInstance] access_token];
	NSLog(@"LONG POLL HISTORY REQUEST %@",[NSString stringWithFormat:@"https://api.vk.com/method/messages.getLongPollHistory?access_token=%@&ts=%@&v=4.94&msgs_limit=200&events_limit=1000", token, newTs]);
    NSURL* requestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.vk.com/method/messages.getLongPollHistory?access_token=%@&ts=%@&v=4.94&msgs_limit=200&events_limit=1000", token, newTs]];
    NSURLRequest* request = [NSURLRequest requestWithURL:requestUrl];
	
    //send the request (will block until a response comes back)
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request
												 returningResponse:&response error:&error];
	//NSLog(@"%@",responseData);
    //pass the response on to the handler (can also check for errors here, if you want)
    [self performSelectorOnMainThread:@selector(dataReceivedHistory:)
						   withObject:responseData waitUntilDone:YES];
	
	NSDictionary *parsedData;
	
	if (responseData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
	} else {
		NSLog(@"Error with %@", error);
		//[self startPoll];
	}
}

- (void) dataReceivedHistory: (NSData*) theData {
    //process the response here
	NSLog(@"process the response here");
	NSLog(@"%@",theData);
	
	NSError *error = nil;
	NSDictionary *parsedData;
	
	if (theData) {
		parsedData = [NSJSONSerialization JSONObjectWithData:theData options:kNilOptions error:&error];
		NSLog(@"%@",parsedData);
	} else {
		NSLog(@"Error with %@", error);
	}
	
	NSLog(@"======================");
}

@end

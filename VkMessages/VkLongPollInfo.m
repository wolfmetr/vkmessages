//
//  VkLongPollInfo.m
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//


#import "VKLongPollInfo.h"
#import "RestKit/RestKit.h"


@implementation VkLongPollInfo

+ (RKObjectMapping *)mapping {
    RKObjectMapping *longPollMapping = [RKObjectMapping mappingForClass:[self class]];
	[longPollMapping addAttributeMappingsFromDictionary:@{
		@"key":		@"key",
		@"server":	@"server",
		@"ts":		@"ts"
	 }];
    return longPollMapping;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\n server: %@ \n key: %@ \n ts: %@ ", self.server, self.key, self.ts];
}

@end
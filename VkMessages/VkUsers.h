//
//  VkUsers.h
//  VkMessages
//
//  Created by baistore on 26.05.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkUser.h"
//#import "VkDialog.h"
//#import "VkDialogs.h"
@interface VkUsers : NSObject

@property (nonatomic, retain) NSMutableArray *users;
@property (nonatomic, retain) NSMutableSet *usersIds;
+(id) initByUids:(NSArray *) uids;
-(NSUInteger) countUsers;
-(void) addUser:(VkUser*) user;
-(void) addUsers:(VkUsers*) usersForAdd;
-(void) addUsersFromSetIds:(NSMutableSet*) idsSet;
//-(void) importUsersFromDialog:(VkDialog*) dialog;
-(VkUser*) getUserByUid:(NSString*) uid;
@end

//
//  DialogsViewController.h
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VkChatViewController.h"

@class VkDialogs;
@class VkMessages;
@interface DialogsViewController : NSObject <NSTableViewDataSource, NSTableViewDelegate>{
	VkDialogs *dialogs;
	NSInteger currentDialogIndex;
}
@property (weak) IBOutlet NSScrollView *scrollView;
@property (weak) IBOutlet NSTextField *messageBoxField;


@property (nonatomic, strong) VkChatViewController *chatViewController;
@property (nonatomic, retain) VkDialogs *dialogs;
- (IBAction)messageBox:(id)sender;
- (void) resetDialogs:(VkDialogs*)dlgs;
- (void) scrollToBottom;

@end

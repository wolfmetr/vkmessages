//
//  VkChatViewController.m
//  VkMessages
//
//  Created by baistore on 07.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkChatViewController.h"
#import "VkDialog.h"
#import "VkDialogs.h"
#import "VkChatCell.h"

@implementation VkChatViewController

@synthesize messages;

-(id)init{
	self = [super init];
    if (self) {
		messages = [[VkDialogs alloc] init];
	}
	
	return self;
}

- (void) resetChat:(VkDialogs*)msgs
{
	[self.messages setDialogs:msgs.dialogs];
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	NSLog(@"CHAT numberOfRowsInTableView %lul",(unsigned long)messages.countDialogs);
	return messages.countDialogs;
	//return 10;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)col row:(NSInteger)row
{

	NSTableCellView *result;
	if (messages.countDialogs){
		VkDialog* msg = [messages.dialogs objectAtIndex:row];
		NSLog(@"msg = %@",msg);
	}

	VkChatCell *cell = [tableView makeViewWithIdentifier:[VkChatCell cellID] owner:self];
	//NSTableCellView *cell = [tableView makeViewWithIdentifier:@"FriendCell" owner:self];
	//VkChatCell *cell = [tableView makeViewWithIdentifier:@"FriendCell" owner:self];
	if (!cell ) {
		cell = [VkChatCell cell];
	}
	
	VkDialog* msg = [messages.dialogs objectAtIndex:messages.countDialogs-row-1];
	[cell.lastMessageLabel setStringValue:msg.body];

	[cell.nameField setStringValue:[NSString stringWithFormat:@"%@ %@",msg.user.firstName,msg.user.lastName]];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd-MM-yyyy"];
	[cell.timeLabel setStringValue:[dateFormatter stringFromDate:msg.date]];
	
	if (msg.user.photo_100){
		NSLog(@"Image url: %@",msg.user.photo_100);
		[cell.imageView downloadImageFromURL:msg.user.photo_100];
	}
		

	result = cell;
	return result;
}

@end

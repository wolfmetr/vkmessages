//
//  VkChatViewController.h
//  VkMessages
//
//  Created by baistore on 07.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VkDialogs;
@interface VkChatViewController : NSObject <NSTableViewDataSource, NSTableViewDelegate> {
	VkDialogs *messages;
}

@property (nonatomic, strong) VkDialogs *messages;

- (void) resetChat:(VkDialogs*)msgs;

@end

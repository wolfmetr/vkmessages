//
//  VkDialogs.m
//  VkMessages
//
//  Created by baistore on 20.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkDialogs.h"

@implementation VkDialogs
@synthesize dialogs;
/*
+(VkDialogs*) getDialogs:(int)limit from:(int)from {
	RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[VkDialog mapping] pathPattern:nil keyPath:@"response" statusCodes:nil];
	NSString *token = [[VkDelegate sharedInstance] access_token];
    NSString *resourcePath = [NSString stringWithFormat:vkGetDialogs, token];
	NSURL *url = [NSURL URLWithString:resourcePath];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	
	RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request
																		responseDescriptors:@[responseDescriptor]];
	
	
	[operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
		NSLog(@"DIALOGS INFO %@", result);

		
	} failure:nil];
	
	[operation start];
}
*/
-(id) init {
	self = [super init];
	if (self){
		dialogs = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id) initWithArray:(NSArray*) arr {
	self = [super init];
	if (self){
		dialogs = [[NSMutableArray alloc] initWithArray:arr];
	}
	return self;
}

-(id) initWithDictionary:(NSDictionary*) dict {
	self = [super init];
	if (self){
		int countNewDialogs = [[dict objectForKey:@"response"] count];
		NSMutableArray *result = [[NSMutableArray alloc] init];
		for (int i = 1; i < countNewDialogs; i++){
			VkDialog *dlg = [[VkDialog alloc] init];
			[dlg setDialog:[[dict objectForKey:@"response"] objectAtIndex:i]];
			[result addObject:dlg];
		}
		dialogs = [[NSMutableArray alloc] initWithArray:result];
	}
	return self;
}

-(NSUInteger) countDialogs
{
	return [dialogs count];
}

-(void) addDialog:(VkDialog *) dlg{
	[dialogs addObject:dlg];
}

-(void) addDialogs:(NSArray *) dlgsArray{
	[dialogs addObjectsFromArray:dlgsArray];
}

-(NSMutableSet*) getUsersIdsSet
{
	NSMutableSet *userIds = [NSMutableSet set];
	for (VkDialog *dialog in dialogs){
		[userIds unionSet:[dialog usersIdsSet]];
	}
	return userIds;
}

-(void) initUsersFormVkUsers:(VkUsers*) dUsers
{
	for (VkDialog *dialog in dialogs){
		[dialog initUsers:dUsers];
	}
}
@end

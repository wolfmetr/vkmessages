//
//  VkDialogs.h
//  VkMessages
//
//  Created by baistore on 20.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkDialog.h"
#import "RestKit/RestKit.h"
//#import "VkDelegate.h"
//#define vkGetDialogs @"https://api.vk.com/method/messages.getDialogs?access_token=%@&filters=%@&count=%@"
@interface VkDialogs : NSObject


@property (nonatomic, retain) NSMutableArray *dialogs;

+(VkDialogs*) getDialogs:(int)limit from:(int)from;
-(NSUInteger) countDialogs;
-(void) addDialog:(VkDialog *) dlg;
-(void) addDialogs:(NSArray *) dlgsArray;
-(NSMutableSet*) getUsersIdsSet;
-(id) initWithArray:(NSArray*) arr;
-(id) initWithDictionary:(NSDictionary*) dict;
-(void) initUsersFormVkUsers:(VkUsers*) dUsers;
@end

//
//  VkLongPollInfo.h
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RKObjectMapping.h>

@interface VkLongPollInfo : NSObject
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *server;
@property (nonatomic, strong) NSString *ts;

+ (RKObjectMapping *)mapping;

@end

//
//  VkDialog.h
//  VkMessages
//
//  Created by baistore on 10.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkUsers.h"
#import "VkMessage.h"

@interface VkDialog : NSObject <NSCoding>{
	BOOL read_state;
	BOOL group;
	NSString *body;
	NSDate *date;
	NSString *mid;
	NSNumber *_out_;
	NSNumber *emoji_;
	NSString *title;
	NSString *uid;
	NSString *admin_id;
	NSString *chat_active;		// список uid пользователей как строка
	NSString *users_count;
	NSString *chat_id;
	VkUser *user;
	VkUsers	*users;
	NSMutableSet *usersIdsSet;	
}

-(void) setDialog:(NSDictionary *)dlg;
-(void) setRead;
-(BOOL) isReaded;
-(BOOL)isMultiDialog;
-(id) initWithDictionary:(NSDictionary *)dlg;
-(void) initUsersIdsSet;
-(void) initUsers:(VkUsers *) dUsers;

@property (nonatomic, assign) BOOL read_state;
@property (nonatomic, assign) BOOL group;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSDate *date;//дата отправки сообщения
@property (nonatomic, strong) NSString *mid;// id сообщения
@property (nonatomic, strong) NSNumber *out_;
@property (nonatomic, strong) NSNumber *emoji_;
@property (nonatomic, strong) NSString *title;// заголовок сообщения или беседы
@property (nonatomic, strong) NSString *uid;// id автора


@property (nonatomic, strong) NSString *admin_id; // id автора беседы
@property (nonatomic, strong) NSString *chat_active; // список участников беседы
@property (nonatomic, strong) VkUsers *users;
@property (nonatomic, strong) VkUser *user;
@property (nonatomic, strong) NSMutableSet *usersIdsSet; // хранится набор uid пользователей
@property (nonatomic, strong) NSString *users_count;
@property (nonatomic, strong) NSString *chat_id; //идентификатор беседы;


@end

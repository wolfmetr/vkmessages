//
//  DialogsViewController.m
//  VkMessages
//
//  Created by baistore on 21.04.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "DialogsViewController.h"
#import "DialogCellView.h"
#import "DialogCell.h"
#import "VkDialog.h"
#import "VkDialogs.h"
#import "VkDataController.h"
@interface DialogsViewController ()

@end

@implementation DialogsViewController
@synthesize dialogs, chatViewController,messageBoxField;

/*
 Действие при нажатии Enter в поле ввода сообщения
 */
- (IBAction)messageBox:(id)sender
{
	if ([[messageBoxField stringValue] length] != 0){
		NSString *msg = [NSString stringWithString:[messageBoxField stringValue]];
		[messageBoxField setStringValue:@""];
		/*
		 отправить сообщение
		 */
		NSLog(@"currentDialogIndex = %ld",(long)currentDialogIndex);
		VkDialog* dlg = [dialogs.dialogs objectAtIndex:currentDialogIndex];
		NSLog(@"dlg = %@", dlg);
		NSString *userString = [NSString stringWithFormat:@"%@",dlg.uid];
		if([dlg isMultiDialog]){
			NSLog(@"isMultiDialog");
			userString = [NSString stringWithFormat:@"%@",dlg.chat_id];
		}
		NSLog(@"Send with params %@ %@ %c", userString,msg, [dlg isMultiDialog]);
		[[VkDataController sharedInstance] sendMessage:userString messageText:msg isChat:[dlg isMultiDialog]];
	}
}


-(id)init{
	self = [super init];
    if (self) {
		dialogs = [[VkDialogs alloc] init];
	}
//	[table1 reloadData];
	
	return self;
}

- (void) resetDialogs:(VkDialogs*)dlgs
{
	[self.dialogs setDialogs:dlgs.dialogs];
	
}


//DialogCellView
- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
	NSLog(@"numberOfRowsInTableView %lul",(unsigned long)dialogs.countDialogs);
	return dialogs.countDialogs;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)col row:(NSInteger)row
{
	NSTableCellView *result;
	DialogCell *cell = [tableView makeViewWithIdentifier:[DialogCell cellID] owner:self];
	if (!cell ) {
		cell = [DialogCell cell];
	}
	
	VkDialog* dlg = [dialogs.dialogs objectAtIndex:row];
	[cell.chatNameLabel setStringValue:dlg.title];
	[cell.lastMessageLabel setStringValue:dlg.body];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd-MM-yyyy"];
	[cell.timeLabel setStringValue:[dateFormatter stringFromDate:dlg.date]];
	if ([dlg.users.users count] > 0 ){
		NSLog(@"Dialog: %@ Image url: %@",dlg.title,[[dlg.users.users objectAtIndex:0] photo_100]);
		if ([[dlg.users.users objectAtIndex:0] photo_100])
			[cell.imageView downloadImageFromURL:[[dlg.users.users objectAtIndex:0] photo_100]];

	}
	result = cell;
	return result;
}

// Действие по выделению диалога
- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)rowIndex {
//	NSLog(@"%li tapped!", (long)rowIndex);
	currentDialogIndex = rowIndex;
	/* 
	 * нужно определить id диалога
	 */
	VkDialog* dlg = [dialogs.dialogs objectAtIndex:rowIndex];
	NSLog(@"%@", dlg);
	
	if([dlg isMultiDialog]){
		NSLog(@"Is multi dialog");
		[[VkDataController sharedInstance] updateDialogHistoryByChatId:dlg.chat_id];
		
	} else {
		NSLog(@"Is single dialog");
		[[VkDataController sharedInstance] updateDialogHistory:dlg.uid];
	}
	
	[messageBoxField setEnabled:YES];
	/*
	 берем айди юзера или мультидиалога и через VkDataConytroller запрашиваем новые данные
	 */
	return YES;
}

- (void)scrollToBottom
{
    NSPoint newScrollOrigin;
	
    // assume that the scrollview is an existing variable
    if ([[self.scrollView documentView] isFlipped]) {
        newScrollOrigin=NSMakePoint(0.0,NSMaxY([[self.scrollView documentView] frame])
									-NSHeight([[self.scrollView contentView] bounds]));
    } else {
        newScrollOrigin=NSMakePoint(0.0,0.0);
    }
	
    [[self.scrollView documentView] scrollPoint:newScrollOrigin];
	
}

@end

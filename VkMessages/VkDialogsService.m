//
//  VkDialogsService.m
//  VkMessages
//
//  Created by baistore on 19.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkDialogsService.h"
#import "VkDialogs.h"
@implementation VkDialogsService

- (id)initWithCompletionBlock:(void (^)(VkDialogs *))arg
{
    self = [super init];
    if (self) {
        self.completionBlock = arg;
    }
    return self;
}

- (void)getDialogs
{
	NSString *tokenParam = [[NSUserDefaults standardUserDefaults] valueForKey:@"vk_token"];
    NSString *path = @"/method/messages.getDialogs";
	NSDictionary *paramsRequest = @{
			   @"count" : @"200",
		 @"access_token": tokenParam,
		 @"fields": @"first_name_nom",
		//@"v":@"4.99"
	  };

	RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://api.vk.com"]];
	[[objectManager HTTPClient] getPath:path
											  parameters:paramsRequest
												 success:^(AFHTTPRequestOperation *operation, id responseObject)
	{
		NSLog(@"MyDialogResponse %@",responseObject);
		VkDialogs *dialogs = [[VkDialogs alloc] initWithDictionary:responseObject];
		
		if (_completionBlock)
			_completionBlock(dialogs);
	}
												 failure:^(AFHTTPRequestOperation *operation, NSError *error)
	{
		NSLog(@"MyDialogResponse failure %@",error);
	}];
}

-(void) getDialogHistoryByUid:(NSString *)userId
{
	[self getDialogHistoryBy:@"uid" withParam:userId];
}
-(void) getDialogHistoryByChatId:(NSString *)chatId
{
	[self getDialogHistoryBy:@"chat_id" withParam:chatId];
}

-(void) getDialogHistoryBy:(NSString *)typeGet withParam:(NSString *) param
{
	NSString *tokenParam = [[NSUserDefaults standardUserDefaults] valueForKey:@"vk_token"];
	//    NSString *path = [NSString stringWithFormat:@"/messages.getHistory?uid=%@&count=200&access_token=%@", userId.stringValue, tokenParam];
	NSString *path = @"/method/messages.getHistory";
	NSDictionary *paramsRequest = @{
		 typeGet : param,
		 @"access_token": tokenParam,
		 @"count" : @"200",
		 @"rev": @"0"
		 };
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://api.vk.com"]];
	[[objectManager HTTPClient] getPath:path
							 parameters:paramsRequest
								success:^(AFHTTPRequestOperation *operation, id responseObject)
	 {
		 NSLog(@"MyDialogHistoryResponse %@",responseObject);
		 
		 VkDialogs *dialogs = [[VkDialogs alloc] initWithDictionary:responseObject];
		 
		 if (_completionBlock)
			 _completionBlock(dialogs);
	 }
								failure:^(AFHTTPRequestOperation *operation, NSError *error)
	 {
		 NSLog(@"MyDialogHistoryResponse failure %@",error);
	 }];
}
@end

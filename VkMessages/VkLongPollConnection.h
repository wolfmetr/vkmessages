//
//  VkLongPullConnection.h
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkDelegate.h"
#import "VkLongPollInfo.h"
#import "VkLongPollResponse.h"
//#import "VkLongPollHistoryResponse.h"
#import "RestKit/RestKit.h"

@protocol VkLongPollDelegateProtocol <NSObject>
- (void)handleResponseWithUpdates:(NSArray *)updates;
@end;


@interface VkLongPollConnection : NSObject
@property(nonatomic) BOOL breakFlag;
@property(nonatomic, retain) NSString *ts;
@property(nonatomic, retain) NSString *key;
@property(nonatomic, retain) NSString *server;
@property(nonatomic, strong) id <VkLongPollDelegateProtocol> delegate;

- (id)initWithDelegate:(id <VkLongPollDelegateProtocol>)delegate;

- (void) restart;
- (void) startPoll;
- (void) longPoll;
- (void) dataReceived: (NSData*) theData;
- (void) longPollHistory:(NSString *)newTs;
- (void) dataReceivedHistory: (NSData*) theData;



@end

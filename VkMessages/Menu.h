//
//  Menu.h
//  VkMessages
//
//  Created by baistore on 31.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menu : NSObject
@property (strong) NSStatusItem *theItem;
@property (strong) NSImage *noMessages;
@property (strong) NSImage *haveMessages;


- (id)initTrayWithMenu:(NSMenu*)appmenu;
- (void)setMessage:(NSString*)mess;
@end

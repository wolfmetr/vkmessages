//
//  VkLongPollResponse.h
//  VkMessages
//
//  Created by baistore on 12.06.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RKObjectMapping.h>


@interface VkLongPollResponse : NSObject

@property (nonatomic, strong) NSString *ts;
@property (nonatomic, strong) NSArray *updates;
@property (nonatomic, strong) NSString *failed;

+ (RKObjectMapping *)mapping;

@end
//
//  VkMessages.m
//  VkMessager
//
//  Created by baistore on 03.12.12.
//  Copyright (c) 2012 wolfmetr. All rights reserved.
//

#import "VkMessages.h"

@implementation VkMessages
@synthesize messages = messages_;

-(id) init {
	self = [super init];
	if (self){
		messages_ = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void) addMessage:(VkMessage *) msg{
	[self.messages addObject:msg];
}

-(NSUInteger) countMessages
{
	return [self.messages count];
}

-(VkMessage*) messageAtIndex:(NSUInteger *) index
{
	return [self.messages objectAtIndex:*index];
}

@end

//
//  VkUsers.m
//  VkMessages
//
//  Created by baistore on 26.05.13.
//  Copyright (c) 2013 wolfmetr. All rights reserved.
//

#import "VkUsers.h"
#import "VkUser.h"
@implementation VkUsers
@synthesize users, usersIds;

-(id) init {
	self = [super init];
	if (self){
		users = [[NSMutableArray alloc] init];
		usersIds = [[NSMutableSet alloc] init];
	}
	return self;
}

// Инициализация по масиву uids
+(id) initByUids:(NSArray *) uids
{
	NSString *str = [NSString stringWithFormat:@"%@",[uids componentsJoinedByString:@","]];
	NSArray *arrDicts = [VkUser loadByUid:str];
	VkUsers *usersResult = [[VkUsers alloc] init];
	for (NSDictionary* d in arrDicts){
		VkUser *user = [VkUser initByDictionary:d];
		[usersResult addUser:user];
	}
	
	return usersResult;
}

-(void) addUsers:(VkUsers*) usersForAdd
{
	for (VkUser *user in usersForAdd.users)
		[self addUser:user];
}

-(void) addUsersFromSetIds:(NSMutableSet*) idsSet
{
	[idsSet minusSet:usersIds];
	NSArray *newIdsSet = [NSArray arrayWithArray:[idsSet allObjects]];
	[ self addUsers:[VkUsers initByUids:newIdsSet]];
}

-(VkUser*) getUserByUid:(NSString*) uid
{
	for (VkUser* user in users) {
		NSString *uid1 = [NSString stringWithFormat:@"%@",user.uid];
		NSString *uid2 = [NSString stringWithFormat:@"%@",uid];
		if ([uid1 isEqualToString:uid2 ]){
			return user;
		}
	}
	
	return NULL;
}


-(void) addUser:(VkUser*) user
{
	if (![self getUserByUid:user.uid]){
		[users addObject:user];
		[usersIds addObject:[NSString stringWithFormat:@"%@",user.uid]];
	}

}

-(NSUInteger) countUsers
{
	return [users count];
}

@end
